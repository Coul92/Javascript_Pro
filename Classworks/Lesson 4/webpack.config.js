'use strict'
const path = require('path');
module.exports = {
    entry: "./js",
    output: {
        filename: "build.js",
        path: path.resolve(__dirname, './build'),
    },
    watch: true,
    devtool: "source-map",
    mode: "development"
}