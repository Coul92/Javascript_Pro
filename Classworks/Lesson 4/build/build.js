/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/index.js":
/*!*********************!*\
  !*** ./js/index.js ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _objectfreeze__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./objectfreeze */ "./js/objectfreeze.js");
/* harmony import */ var _singleton__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./singleton */ "./js/singleton.js");



Object(_objectfreeze__WEBPACK_IMPORTED_MODULE_0__["default"])();

_singleton__WEBPACK_IMPORTED_MODULE_1__["default"].addLaw('В городе Элко в Неваде по улицам можно ходить только в маске.');
_singleton__WEBPACK_IMPORTED_MODULE_1__["default"].addLaw('В Айдахо запрещено рыбачить, сидя на верблюде.');
_singleton__WEBPACK_IMPORTED_MODULE_1__["default"].addLaw('В Гонолулу на Гавайях считается преступлением "приставать к птицам" в городских парках. ');
_singleton__WEBPACK_IMPORTED_MODULE_1__["default"].readConstitution();
_singleton__WEBPACK_IMPORTED_MODULE_1__["default"].showCitizensSatisfactions();
_singleton__WEBPACK_IMPORTED_MODULE_1__["default"].showBudget();
_singleton__WEBPACK_IMPORTED_MODULE_1__["default"].doCelebration();



/***/ }),

/***/ "./js/objectfreeze.js":
/*!****************************!*\
  !*** ./js/objectfreeze.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  let universe = {
    infinity: Infinity,
    good: ['cats', 'love', 'rock-n-roll'],
    evil: {
      bonuses: ['cookies', 'great look']
    }
  };

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // true
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/
let universe = {
  infinity: Infinity,
  good: ['cats', 'love', 'rock-n-roll'],
  evil: {
    bonuses: ['cookies', 'great look']
  }
};

function iterate(obj) {
  for (var property in obj) {
    if (obj.hasOwnProperty(property)) {
      if (typeof obj[property] == "object")
        iterate(obj[property]);
      else {
        Object.freeze(obj[property]);
      }
    }
  }
  Object.freeze(obj);
  return obj;
}



const work = () => {
  try {
    let FarGalaxy = iterate(universe);

    FarGalaxy.good.push('javascript');

    FarGalaxy.evil.humans = [];
  } catch (e) {
    console.log(e);
  }
}

/* harmony default export */ __webpack_exports__["default"] = (work);


/***/ }),

/***/ "./js/singleton.js":
/*!*************************!*\
  !*** ./js/singleton.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

class Law {
  constructor(id, description) {
    this.id = id;
    this.description = description;
  }
}

const data = {
  laws: [],
  budget: 1000000,
  citizensSatisfactions: 0,
}

const government = {
  
  addLaw(description) {
    let law = new Law(data.laws.length, description);
    data.laws.push(law);
    data.citizensSatisfactions -= 10;
  },
  readConstitution() {
    data.laws.forEach(law => this.readLaw(law.id));
  },
  readLaw(id) {
    let law = data.laws.find(l => l.id === id);
    console.log(`Law #${law.id}\n${law.description}`)
  },
  showCitizensSatisfactions() {
    console.log(`Citizens Satisfactions level is ${data.citizensSatisfactions}`);
  },
  showBudget() {
    console.log(`Budget is $${data.budget}`);
  },
  doCelebration() {
    data.budget -= 50000;
    data.citizensSatisfactions += 5;
    console.log('Making celebration...');
    this.showCitizensSatisfactions();
    this.showBudget();
  }
}

Object.freeze(government);

/* harmony default export */ __webpack_exports__["default"] = (government);


/***/ })

/******/ });
//# sourceMappingURL=build.js.map