import { default as objectfreeze } from './objectfreeze';
import { default as government } from './singleton';

objectfreeze();

government.addLaw('В городе Элко в Неваде по улицам можно ходить только в маске.');
government.addLaw('В Айдахо запрещено рыбачить, сидя на верблюде.');
government.addLaw('В Гонолулу на Гавайях считается преступлением "приставать к птицам" в городских парках. ');
government.readConstitution();
government.showCitizensSatisfactions();
government.showBudget();
government.doCelebration();

