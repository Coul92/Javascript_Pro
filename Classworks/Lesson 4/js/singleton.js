/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/

class Law {
  constructor(id, description) {
    this.id = id;
    this.description = description;
  }
}

const data = {
  laws: [],
  budget: 1000000,
  citizensSatisfactions: 0,
}

const government = {
  
  addLaw(description) {
    let law = new Law(data.laws.length, description);
    data.laws.push(law);
    data.citizensSatisfactions -= 10;
  },
  readConstitution() {
    data.laws.forEach(law => this.readLaw(law.id));
  },
  readLaw(id) {
    let law = data.laws.find(l => l.id === id);
    console.log(`Law #${law.id}\n${law.description}`)
  },
  showCitizensSatisfactions() {
    console.log(`Citizens Satisfactions level is ${data.citizensSatisfactions}`);
  },
  showBudget() {
    console.log(`Budget is $${data.budget}`);
  },
  doCelebration() {
    data.budget -= 50000;
    data.citizensSatisfactions += 5;
    console.log('Making celebration...');
    this.showCitizensSatisfactions();
    this.showBudget();
  }
}

Object.freeze(government);

export default government;
