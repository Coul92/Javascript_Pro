/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/src/decorator.js":
/*!*****************************!*\
  !*** ./js/src/decorator.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
const decorator = () => {

    console.log('DECORATOR ON CLASSES!');

    function LogTemperature(changeValue) {
        console.log(
            '\nprevious', this.currentTemperature,
            '\ncurrent', this.currentTemperature + changeValue,
            '\nmin', this.minTemperature,
            this.maxTemperature !== undefined && '\nmax', this.maxTemperature
        );
    }

    class Human {
        constructor(name) {
            this.name = name;
            this.currentTemperature = 0;
            this.minTemperature = -10;

            console.log(`new Human ${this.name} arrived!`, this);
        }

        changeTemperature(changeValue) {
            LogTemperature.call(this, changeValue);
            // let prevTemperature = this.currentTemperature;
            this.currentTemperature = this.currentTemperature + changeValue;

            // if (this.currentTemperature < this.minTemperature) {
            //     console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
            // } else {
            //     if (this.currentTemperature > prevTemperature) {
            //         console.log(`Temperature is growing. Seems someone go to Odessa or drink some hot tea?`)
            //     } else {
            //         console.log(`It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);
            //     }
            // }
            this.checkCondition(this.currentTemperature <= this.minTemperature, `Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
            // this.checkCondition( this.currentTemperature > prevTemperature, `Temperature is growing. Seems someone go to Odessa or drink some hot tea?`);
            // this.checkCondition( this.currentTemperature < prevTemperature , `It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);
        }

        checkCondition(condition, output) {
            if (condition) {
                console.log(output);
            }
        }
    }

    class Cooler {
        constructor(name, temperatureCoolRate) {
            this.name = name;
            this.temperatureCoolRate = temperatureCoolRate;
        }
    }

    class CooledHuman extends Human {
        constructor(name) {
            super(name);
            this.coolers = [];
            this.addCooler(new Cooler('icecream', -5)),
            this.addCooler(new Cooler('cold water', -5)),
            this.addCooler(new Cooler('cooler', -5)),
            this.addCooler(new Cooler('heart of your ex-girlfriend', -5))
            this.maxTemperature = 30;
        }

        changeTemperature(changeValue) {
            super.changeTemperature(changeValue);
            if (this.currentTemperature > this.maxTemperature) {
                let cooler = this.coolers.pop();
                if (cooler) {
                    console.log(`take this ${cooler.name} and cool down!`);
                    this.changeTemperature(cooler.temperatureCoolRate);
                    return;
                }

            }
            super.checkCondition(this.currentTemperature > 50, `${this.name} зажарился на солнце :(`);

        }

        addCooler(Cooler) {
            if (!Cooler.name || !Cooler.temperatureCoolRate) {
                throw new Error('It\'s not a cooler!');
            }
            this.coolers.push(Cooler);
            console.log(`Cooler ${Cooler.name} with temparature cool rate ${Cooler.temperatureCoolRate} was added!`);
        }
    }

    let Debra = new CooledHuman('Debra');
    Debra.changeTemperature(100);

}

/* harmony default export */ __webpack_exports__["default"] = (decorator);

/***/ }),

/***/ "./js/src/index.js":
/*!*************************!*\
  !*** ./js/src/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _decorator__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./decorator */ "./js/src/decorator.js");
/* harmony import */ var _mediator__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./mediator */ "./js/src/mediator.js");



// decorator();
Object(_mediator__WEBPACK_IMPORTED_MODULE_1__["default"])();

/***/ }),

/***/ "./js/src/mediator.js":
/*!****************************!*\
  !*** ./js/src/mediator.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*
  Написать медиатор для группы студентов.

  Профессор отвечает только на вопросы старосты.

  У Студента есть имя и группа которой он принадлежит.
  Он может запросить старосту задать вопрос или получить ответ.

  Староста может добавлять студентов в группу и передавать вопрос профессору.
*/

const Mediator = () => {
    class Professor {
      answerTheQuestion( student, question ){
        if( student.type !== 'monitor'){
          console.error('It\' not your bussines');
        } else {
          console.log('Yes, my dear?!');
        }
      }
    }
  
    class Student {
      constructor(name, monitor){
          this.name = name;
          this.monitor = monitor
      }
      getAnswer(question) {
          this.monitor.askProfessor(question, this);
      }
      tipTheMonitor(){}
    }
  
    // Monitor == Староста
    class Monitor extends Student{
      constructor(name, professor){
          super(name);
          this.students = [];
          this.type = 'monitor';
          this.professor = professor;
      }
      addToGroup(student){
          this.students.push(student);
      }
      askProfessor(question, student) {
          if (this.students.find(s => s === student)) {
            this.professor.answerTheQuestion(this, question);
          } else {
            console.log('You are not in my group!');
          }
      }
    }

    let professor = new Professor();
    let monitor = new Monitor('Debra', professor);
    let student1 = new Student('Dexter', monitor);
    let student2 = new Student('John', monitor);
    monitor.addToGroup(student1);
    monitor.addToGroup(student2);

    student1.getAnswer('Как пропатчить  kde2 под freebsd?');
  }
  
  /* harmony default export */ __webpack_exports__["default"] = (Mediator);

/***/ })

/******/ });
//# sourceMappingURL=build.js.map