const decorator = () => {

    console.log('DECORATOR ON CLASSES!');

    function LogTemperature(changeValue) {
        console.log(
            '\nprevious', this.currentTemperature,
            '\ncurrent', this.currentTemperature + changeValue,
            '\nmin', this.minTemperature,
            this.maxTemperature !== undefined && '\nmax', this.maxTemperature
        );
    }

    class Human {
        constructor(name) {
            this.name = name;
            this.currentTemperature = 0;
            this.minTemperature = -10;

            console.log(`new Human ${this.name} arrived!`, this);
        }

        changeTemperature(changeValue) {
            LogTemperature.call(this, changeValue);
            // let prevTemperature = this.currentTemperature;
            this.currentTemperature = this.currentTemperature + changeValue;

            // if (this.currentTemperature < this.minTemperature) {
            //     console.error(`Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
            // } else {
            //     if (this.currentTemperature > prevTemperature) {
            //         console.log(`Temperature is growing. Seems someone go to Odessa or drink some hot tea?`)
            //     } else {
            //         console.log(`It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);
            //     }
            // }
            this.checkCondition(this.currentTemperature <= this.minTemperature, `Temperature is to low: ${this.currentTemperature}. ${this.name} died :(`);
            // this.checkCondition( this.currentTemperature > prevTemperature, `Temperature is growing. Seems someone go to Odessa or drink some hot tea?`);
            // this.checkCondition( this.currentTemperature < prevTemperature , `It's cold outside (${this.currentTemperature} deg), please wear some clothes, or ${this.name} will die!`);
        }

        checkCondition(condition, output) {
            if (condition) {
                console.log(output);
            }
        }
    }

    class Cooler {
        constructor(name, temperatureCoolRate) {
            this.name = name;
            this.temperatureCoolRate = temperatureCoolRate;
        }
    }

    class CooledHuman extends Human {
        constructor(name) {
            super(name);
            this.coolers = [];
            this.addCooler(new Cooler('icecream', -5)),
            this.addCooler(new Cooler('cold water', -5)),
            this.addCooler(new Cooler('cooler', -5)),
            this.addCooler(new Cooler('heart of your ex-girlfriend', -5))
            this.maxTemperature = 30;
        }

        changeTemperature(changeValue) {
            super.changeTemperature(changeValue);
            if (this.currentTemperature > this.maxTemperature) {
                let cooler = this.coolers.pop();
                if (cooler) {
                    console.log(`take this ${cooler.name} and cool down!`);
                    this.changeTemperature(cooler.temperatureCoolRate);
                    return;
                }

            }
            super.checkCondition(this.currentTemperature > 50, `${this.name} зажарился на солнце :(`);

        }

        addCooler(Cooler) {
            if (!Cooler.name || !Cooler.temperatureCoolRate) {
                throw new Error('It\'s not a cooler!');
            }
            this.coolers.push(Cooler);
            console.log(`Cooler ${Cooler.name} with temparature cool rate ${Cooler.temperatureCoolRate} was added!`);
        }
    }

    let Debra = new CooledHuman('Debra');
    Debra.changeTemperature(100);

}

export default decorator;