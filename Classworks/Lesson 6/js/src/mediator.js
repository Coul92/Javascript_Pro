/*
  Написать медиатор для группы студентов.

  Профессор отвечает только на вопросы старосты.

  У Студента есть имя и группа которой он принадлежит.
  Он может запросить старосту задать вопрос или получить ответ.

  Староста может добавлять студентов в группу и передавать вопрос профессору.
*/

const Mediator = () => {
    class Professor {
      answerTheQuestion( student, question ){
        if( student.type !== 'monitor'){
          console.error('It\' not your bussines');
        } else {
          console.log('Yes, my dear?!');
        }
      }
    }
  
    class Student {
      constructor(name, monitor){
          this.name = name;
          this.monitor = monitor
      }
      getAnswer(question) {
          this.monitor.askProfessor(question, this);
      }
      tipTheMonitor(){}
    }
  
    // Monitor == Староста
    class Monitor extends Student{
      constructor(name, professor){
          super(name);
          this.students = [];
          this.type = 'monitor';
          this.professor = professor;
      }
      addToGroup(student){
          this.students.push(student);
      }
      askProfessor(question, student) {
          if (this.students.find(s => s === student)) {
            this.professor.answerTheQuestion(this, question);
          } else {
            console.log('You are not in my group!');
          }
      }
    }

    let professor = new Professor();
    let monitor = new Monitor('Debra', professor);
    let student1 = new Student('Dexter', monitor);
    let student2 = new Student('John', monitor);
    monitor.addToGroup(student1);
    monitor.addToGroup(student2);

    student1.getAnswer('Как пропатчить  kde2 под freebsd?');
  }
  
  export default Mediator;