'use strict'
const path = require('path');
module.exports = {
    entry: './js/src',
    output: {
        filename: 'build.js',
        path: path.resolve(__dirname, './js/build')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        plugins: [
                            ['@babel/plugin-proposal-decorators', { 'legacy': true }]
                        ]
                    }
                }
            }
        ]
    },
    watch: true,
    devtool: 'source-map',
    mode: 'development'
}