/*
  Задание: Модуль создания плейлиста, используя паттерн Обсервер.

  У вас есть данные о исполнителях и песнях. Задание делится на три модуля:
    1. Список исполнителей и песен (Находится слева) - отуда можно включить
    песню в исполнение иди добавить в плейлист.
    Если песня уже есть в плейлисте, дважды добавить её нельзя.

    2. Плейлист (Находится справа) - список выбраных песен, песню можно удалить,
    или запустить в исполнение. Внизу списка должен выводиться блок, в котором
    пишет суммарное время проигрывания всех песен в плейлисте.

    3. Отображает песню которая проигрывается.

    4. + Бонус: Сделать прогресс пар того как проигрывается песня
    с возможностью его остановки.
*/
import moment from 'moment';
import momentDurationFormatSetup from 'moment-duration-format';

const MusicBox = () => {

  class Playlist {
    constructor(divPlaylist, divTotalTime) {
      this.divPlaylist = divPlaylist;
      this.artists = [];
      this.divTotalTime = divTotalTime;
      this.totalTime = moment.duration(0);
    }

    get currentSong() {
      return this._currentSong;
    }

    set currentSong(value) {
      if (this._currentSong) {
        this._currentSong.divSong.classList.remove('playing');
      }
      value.divSong.classList.add('playing');
      this._currentSong = value;
    }

    addArtist(artist) {
      this.artists.push(artist);
      let divArtist = document.createElement('div');
      divArtist.dataset.artist = artist.name;
      divArtist.classList.add('artist');
      let divArtistName = document.createElement('div');
      divArtistName.classList.add('artist-name');
      divArtistName.innerText = artist.name;
      divArtist.appendChild(divArtistName);
      let divArtistSongs = document.createElement('div');
      divArtistSongs.classList.add('artist-songs');
      this.divPlaylist.appendChild(divArtist);
      artist.divArtist = divArtist;
      artist.playlist = this;
    }

    removeArtist(artist) {
      this.artists = this.artists.filter(a => a.name !== artist.name);
      this.divPlaylist.removeChild(artist.divArtist);
    }

    containsArtist(artistName) {
      return this.artists.findIndex(a => a.name === artistName) > -1;
    }

    getArtist(artistName) {
      return this.artists.find(a => a.name === artistName);
    }

    addTotalTime(time) {
      this.totalTime = this.totalTime.add(moment.duration(time[0], "minutes"));
      this.totalTime = this.totalTime.add(moment.duration(time[1], "seconds"));
      this.divTotalTime.innerHTML = this.totalTime.format();
    }

    removeTotalTime(time) {
      this.totalTime = this.totalTime.subtract(moment.duration(time[0], "minutes"));
      this.totalTime = this.totalTime.subtract(moment.duration(time[1], "seconds"));
      this.divTotalTime.innerHTML = this.totalTime.format();
    }
  }

  class Artist {
    constructor(name) {
      this.name = name;
      this.songs = [];
    }

    addSong(song) {
      this.songs.push(song);
      let divSong = document.createElement('div');
      divSong.dataset.artist = this.name;
      divSong.dataset.id = song.id;
      let divSongName = document.createElement('div');
      divSongName.classList.add('song-name');
      divSongName.innerHTML = song.name;
      divSong.appendChild(divSongName);
      let divSongDuration = document.createElement('div');
      divSongDuration.innerText = song.duration.format();
      divSong.appendChild(divSongDuration);
      divSong.classList.add('song');
      divSong.onclick = song.onclick;
      song.actions.forEach(action => {
        let button = document.createElement('button');
        button.innerHTML = action.title.toString();
        button.onclick = action.fn;
        divSong.appendChild(button);
      });
      song.divSong = divSong;
      this.divArtist.appendChild(divSong);
      this.playlist.addTotalTime(song.time);
    }

    removeSong(song) {
      this.songs = this.songs.filter(s => s.id !== song.id);
      this.divArtist.removeChild(song.divSong);
      this.playlist.removeTotalTime(song.time);
    }

    containsSong(songId) {
      return this.songs.findIndex(s => s.id === songId) > -1;
    }
  }

  class Song {
    constructor(id, name, time, artist) {
      this.id = id;
      this.name = name;
      this.time = time;
      this.artist = artist;
      this.actions = [];
      this.duration = moment.duration(time[0], "minutes");
      this.duration =  this.duration.add(moment.duration(time[1], "seconds"));
    }

    addAction(action) {
      this.actions.push(action);
    }
  }

  let progress = document.querySelector('#progress');
  let progressInterval;

  const MusicList = [
    {
      title: 'Rammstein',
      songs: [
        {
          id: 1,
          name: 'Du Hast',
          time: [3, 12]
        },
        {
          id: 2,
          name: 'Ich Will',
          time: [5, 1]
        },
        {
          id: 3,
          name: 'Mutter',
          time: [4, 15]
        },
        {
          id: 4,
          name: 'Ich tu dir weh',
          time: [5, 13]
        },
        {
          id: 5,
          name: 'Rammstein',
          time: [3, 59]
        }
      ]
    },
    {
      title: 'System of a Down',
      songs: [
        {
          id: 6,
          name: 'Toxicity',
          time: [4, 22]
        },
        {
          id: 7,
          name: 'Sugar',
          time: [2, 44]
        },
        {
          id: 8,
          name: 'Lonely Day',
          time: [3, 19]
        },
        {
          id: 9,
          name: 'Lost in Hollywood',
          time: [5, 9]
        },
        {
          id: 10,
          name: 'Chop Suey!',
          time: [2, 57]
        }
      ]
    },
    {
      title: 'Green Day',
      songs: [
        {
          id: 11,
          name: '21 Guns',
          time: [4, 16]
        },
        {
          id: 12,
          name: 'Boulevard of broken dreams!',
          time: [6, 37]
        },
        {
          id: 13,
          name: 'Basket Case!',
          time: [3, 21]
        },
        {
          id: 14,
          name: 'Know Your Enemy',
          time: [4, 11]
        }
      ]
    },
    {
      title: 'Linkin Park',
      songs: [
        {
          id: 15,
          name: 'Numb',
          time: [3, 11]
        },
        {
          id: 16,
          name: 'New Divide',
          time: [4, 41]
        },
        {
          id: 17,
          name: 'Breaking the Habit',
          time: [4, 1]
        },
        {
          id: 18,
          name: 'Faint',
          time: [3, 29]
        }
      ]
    }
  ]

  const MusicBox = document.getElementById('MusicBox');
  const MusicPlayList = document.getElementById('MusicPlayList');
  let playlistLeft = new Playlist(MusicBox, MusicBox.parentNode.querySelector('.MusicTotal'));
  let playlistRight = new Playlist(MusicPlayList, MusicPlayList.parentNode.querySelector('.MusicTotal'));
  MusicList.forEach(artistData => {
    let artist = new Artist(artistData.title);
    playlistLeft.addArtist(artist);
    artistData.songs.forEach(songData => {
      let song = new Song(songData.id, songData.name, songData.time, artist);
      song.addAction(
        {
          title: "add",
          fn: function () {
            let artistNew;
            if (!playlistRight.containsArtist(artist.name)) {
              artistNew = new Artist(artist.name);
              playlistRight.addArtist(artistNew);
            } else {
              artistNew = playlistRight.getArtist(artist.name);
            }
            let songNew = new Song(songData.id, songData.name, songData.time, artist);
            songNew.addAction({
              title: "remove", fn: function () {
                artistNew.removeSong(songNew);
                if (artistNew.songs.length === 0) {
                  playlistRight.removeArtist(artistNew);
                }
              }
            });
            songNew.addAction({
              title: "play", fn: function () {
                playlistRight.currentSong = songNew;
                clearInterval(progressInterval);
                let MusicPlaying = document.querySelector('#MusicPlaying');
                let song__name = MusicPlaying.querySelector('.song__name');
                let song__creator = MusicPlaying.querySelector('.song__creator');
                let song__duration = MusicPlaying.querySelector('.song__duration');
                song__name.innerText = songNew.name;
                song__creator.innerText = artistNew.name;
                song__duration.innerText = songNew.duration.format();
                progress.value = 0;
                progress.max = songNew.duration.asMilliseconds();
                progressInterval = setInterval(function() {
                  if (progress.value < progress.max) {
                    let step = songNew.duration.asMilliseconds() / 100;
                    progress.value += step;
                  }
                }, 1000);
              }
            });
            if (!artistNew.containsSong(song.id)) {
              artistNew.addSong(songNew);
            }
          }
        });
      artist.addSong(song);
    });
  });

  btnPause.onclick = function(e) {
    clearInterval(progressInterval);
  }

  btnPlay.onclick = function(e) {
    progressInterval = setInterval(function() {
      if (progress.value < progress.max) {
        let step = playlistRight.currentSong.duration.asMilliseconds() / 100;
        progress.value += step;
      }
    }, 1000);
  }
}

export default MusicBox;
