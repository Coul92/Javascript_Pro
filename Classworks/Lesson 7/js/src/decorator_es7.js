/*

  Задание:
    1. Используя функциональный декоратор, написать декоратор который будет показывать
       аргументы и результат выполнения функции.

    2. Написать декоратор для класса, который будет преобразовывать аргументы в число,
       если они переданы строкой, и выводить ошибку если переданая переменная не
       может быть преобразована в число
*/

const Work1 = () => {

  function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

  function checkIsNumber(target, key, descriptor) {
    const originFn = descriptor.value;
    descriptor.value = function(...args) {
      args.forEach(arg => {
        if (!isNumeric(arg)) {
          throw new Error(`Аргумент ${arg} не является числом!`);
        }
      });
      originFn.apply(this, args);
      return this;
    }
  }

  class CoolMath {
    @checkIsNumber
    addNumbers(a,b){ return a+b; }
    @checkIsNumber
    multiplyNumbers(a,b){ return a*b}
    @checkIsNumber
    minusNumbers(a,b){ return a-b }
  }
  let Calcul = new CoolMath();
  let x = Calcul.addNumbers(2, 2)
  let y = Calcul.multiplyNumbers(10, 2)
  let z = Calcul.minusNumbers(10, 2);
  let test = Calcul.addNumbers(100, 'test');
  console.log(x, y, z);
};

export default Work1;
