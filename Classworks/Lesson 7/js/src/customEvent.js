/*
  Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
            соответсвии с правилавми ниже:

  1. Написать кастомные события которые будут менять статус светофора:
  - start: включает зеленый свет
  - stop: включает красный свет
  - night: включает желтый свет, который моргает с интервалом в 1с.
  И зарегистрировать каждое через addEventListener на каждом из светофоров.

  2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
      чтобы включить режим "нерегулируемого перекрестка" (моргающий желтый).

  3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)
      или зеленый (на второй клик) цвет соотвественно.
      Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

  4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
      статуса, на мигающий желтые.
      Двойной, тройной и более клики на кнопку не должны вызывать повторную
      инициализацию инвервала.

*/

const CustomEvents = () => {
    const red = 'red';
    const green = 'green';
    const yellow = 'yellow';
    let eventStart = new CustomEvent('start');
    let eventStop = new CustomEvent('stop');
    let eventNight = new CustomEvent('night');
    let trafficLights = document.querySelectorAll('.trafficLight');
    trafficLights.forEach(trafficLight => {
        let interval;
        let circles = trafficLight.getElementsByClassName('trafficLight__circle');
        trafficLight.addEventListener('start', function(e) {
            
            circles[0].style.backgroundColor = '';
            circles[2].style.backgroundColor = trafficLight.status = 'green';
        });
        trafficLight.addEventListener('stop', function(e) {
            circles[2].style.backgroundColor = '';
            circles[0].style.backgroundColor = trafficLight.status = 'red';
        });
        trafficLight.addEventListener('night', function(e) {
            document.body.style.backgroundColor = 'black';
            circles[0].style.backgroundColor = '';
            circles[2].style.backgroundColor = '';
            trafficLight.status = yellow;
            clearInterval(interval);
            interval = setInterval(function() {
                if (circles[1].style.backgroundColor === yellow) {
                    circles[1].style.backgroundColor = '';
                } else if (circles[1].style.backgroundColor === '') {
                    circles[1].style.backgroundColor = yellow;
                }
            }, 1000);
        })
        trafficLight.addEventListener('click', function(e) {
            document.body.style.backgroundColor = '';
            if (!trafficLight.status) {
                circles[0].style.backgroundColor = trafficLight.status = 'red';
            } else {
                if (trafficLight.status === red) {
                    trafficLight.dispatchEvent(eventStart);
                } else if (trafficLight.status === green) {
                    trafficLight.dispatchEvent(eventStop);
                } else if (trafficLight.status === yellow) {
                    clearInterval(interval);
                    circles[1].style.backgroundColor = '';
                    trafficLight.dispatchEvent(eventStop);
                }
            }
        });
        trafficLight.dispatchEvent(eventNight);
    });

    let btnNight = document.getElementById('Do');
    btnNight.addEventListener('click', function(e) {
        trafficLights.forEach(trafficLight => {
            trafficLight.dispatchEvent(eventNight);
        })
    });
};

export default CustomEvents;
