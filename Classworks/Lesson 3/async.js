document.addEventListener('DOMContentLoaded', async function(e) {
    var response = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
    var json = await response.json();
    let output = document.getElementById("async");

    let table = document.createElement('table');
    table.border = '1';
    let tr = document.createElement('tr');

    let thCompany = document.createElement('th');
    thCompany.innerHTML = 'Company';

    let thBalance = document.createElement('th');
    thBalance.innerHTML = 'Balance';

    let thDateRegistered = document.createElement('th');
    thDateRegistered.innerHTML = 'Показать дату регистрации';

    let thAddress = document.createElement('th');
    thAddress.innerHTML = 'Показать адрес';

    tr.appendChild(thCompany);
    tr.appendChild(thBalance);
    tr.appendChild(thDateRegistered);
    tr.appendChild(thAddress);

    table.appendChild(tr);

    for(let i = 0; i < json.length; i++) {
        let tr = document.createElement('tr');

        let tdCompany = document.createElement('td');
        tdCompany.innerHTML = json[i].company;

        let tdBalance = document.createElement('td');
        tdBalance.innerHTML = json[i].balance;

        let tdDateRegistered = document.createElement('td');
        let aDateRegistered = document.createElement('a');
        aDateRegistered.href = '#';
        aDateRegistered.innerHTML = 'Показать дату регистрации';
        aDateRegistered.onclick = function(e) {
            e.preventDefault();
            tdDateRegistered.innerHTML = json[i].registered;
        }
        tdDateRegistered.appendChild(aDateRegistered);

        let tdAddress = document.createElement('td');
        let aAddress = document.createElement('a');
        aAddress.href = '#';
        aAddress.innerHTML = 'Показать адрес';
        aAddress.onclick = function(e) {
            e.preventDefault();
            tdAddress.innerHTML = `City: ${json[i].address.city}<br>
                                   Country: ${json[i].address.coutry}<br>
                                   House: ${json[i].address.house}<br>
                                   State: ${json[i].address.state}<br>
                                   Street: ${json[i].address.street}<br>
                                   Zip: ${json[i].address.zip}`;
        }
        tdAddress.appendChild(aAddress);

        tr.appendChild(tdCompany);
        tr.appendChild(tdBalance);
        tr.appendChild(tdDateRegistered);
        tr.appendChild(tdAddress);

        table.appendChild(tr);
    }


    output.appendChild(table);
});