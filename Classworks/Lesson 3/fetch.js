document.addEventListener('DOMContentLoaded', function() {
    let url = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
    const getJson = (response) => response.json();
    fetch(url)
    .then(getJson)
    .then(json => json[Math.floor(Math.random() * json.length)])
    .then( person => {
        let url = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';
        return fetch(url)
        .then(getJson)
        .then( friends => {
            return {
                name: person.name,
                age: person.age,
                friends: friends[0].friends 
            }
        });

    })
    .then( FinalPerson => {
        let output = document.getElementById('fetch');
        console.log( FinalPerson );

        let h1 = document.createElement('h1');
        h1.innerText = `${FinalPerson.name}, age ${FinalPerson.age}, friends:`;

        let ul = document.createElement('ul');

        for(let i = 0; i < FinalPerson.friends.length; i++) {
            let li = document.createElement('li');
            li.innerText =FinalPerson.friends[i].name;

            ul.appendChild(li);
        }

        output.appendChild(h1);
        output.appendChild(ul);
    });
});

