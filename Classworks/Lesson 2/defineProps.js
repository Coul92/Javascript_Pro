/*
  Задание:

  Написать класс SuperDude который как аргумент принимает два параметра:
    - Имя
    - Массив суперспособностей которые являются обьектом.

    Модель суперспособности:
      {
        // Имя способности
        name:'Invisibility',
        // Сообщение которое будет выведено когда способность была вызвана
        spell: function(){ return `${this.name} hide from you`}
      }

    В конструкторе, нужно:
    - сделать так, что бы имя нельзя было перезаписывать и присвоить ему то
      значение которое мы передали как аргумент.

    - перебрать массив способностей и на каждую из них создать метод для этого
      обьекта, используя поле name как название метода, а spell как то,
      что нужно вернуть в console.log при вызове этого метода.
    - все способности должны быть неизменяемые

    - бонус, создать конструктор суперспособностей -> new Spell( name, spellFunc );
*/

class Spell {
    constructor(name, spellFunc) {
        this.name = name;
        this.spellFunc = spellFunc;
    }
}

class SuperDude {
    constructor(name, superPowers) {
        this._name = name;
        this._superPowers = superPowers;

        Object.defineProperty(this, 'name', {
            value: this._name,
            writable: false
        });

        Object.defineProperty(this, 'superPowers', {
            value: this._superPowers,
            writable: false
        });

        this._superPowers.forEach(s => {
            this[s.name] = s.spellFunc.bind(this);
        });
    }
}

let superPowers = [
    new Spell('Invisibility', function() {
        return `${this.name} hide from you`;
    }),
    new Spell('superSpeed', function() {
        return `${this.name} running from you`;
    }),
    new Spell('superSight', function() {
        return `${this.name} see you`;
    }),
    new Spell('superFroze', function() {
        return `${this.name} will froze you`;
    }),
    new Spell('superSkin', function() {
        return `${this.name} skin is unbreakable`;
    })
];

let Luther = new SuperDude('Luther', superPowers);
    console.log(Luther);

    // Тестирование: Методы должны работать и выводить сообщение.
    console.log(Luther.superSight());
    console.log(Luther.superSpeed());
    console.log(Luther.superFroze());
    console.log(Luther.Invisibility());
    console.log(Luther.superSkin());


