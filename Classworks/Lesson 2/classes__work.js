/*
  Задание:

    I. Написать класс Post который будет уметь:

      1. Конструктор:
          title
          image
          description
          likes

      2. Методы
          render -> отрисовать элемент в ленте
          likePost -> увеличивает счетчик постов
          Методы для изменения title, image, description
          + бонус. Сделать получение и изменение через set и get

    II. Написать класс Advertisment который будет экстендится от класа Post
        но помимо будет иметь другой шаблон вывода, а так же метод для покупки обьекта

        buyItem -> выведет сообщение что вы купили обьект

    III.  Сгенерировать ленту из всех постов что вы добавили в систему.
          Каждый третий пост должен быть рекламным
*/

posts = [];

class Post {
    constructor(title, image, description, likes) {
        this.title = title;
        this.image = image;
        this.description = description;
        this.likes = likes;
        
        posts.push(this);
    }

    render(feed) {
        let post = document.createElement('div');
        this.post = post;
        post.classList.add('post');
        
        let h2Title = document.createElement("h2");
        h2Title.innerText = this.title;
        post.appendChild(h2Title);

        let image = document.createElement("img");
        image.src = this.image;
        image.style.height = '100px';
        post.appendChild(image);

        let pDescription = document.createElement('p');
        pDescription.innerText = this.description;
        post.appendChild(pDescription);

        let spanLikes = document.createElement('span');
        spanLikes.innerText = this.likes.toString();
        this.spanLikes = spanLikes;
        post.appendChild(spanLikes);

        let btnLike = document.createElement('button');
        btnLike.innerHTML = 'Like';
        btnLike.onclick = this.likePost.bind(this);
        post.appendChild(btnLike);

        feed.appendChild(post);
    }

    likePost() {
        this.likes++;
        this.spanLikes.innerText = this.likes.toString();
    }
}

class Advertisment extends Post {
    constructor() {
        super("Dave's hot'n juicy cheesburgers", 'images/adv.JPG', "It's our classic the way Dave intended! A juicy quarter pound of fresh, never frozen beef decorated with premium toppings all between a warm toasted bun.", 100500);
    }

    render(feed) {
        super.render(feed);
        this.post.classList.add('adv');
        this.post.onclick = () => {
            window.open("https://menu.wendys.com/en_US/product/daves-single/");
        }
    }
}

let post1 = new Post('Sit amet mauris commodo quis',
                     'images/post1.jpg',
                     'Facilisis sed odio morbi quis commodo odio aenean sed adipiscing diam donec adipiscing tristique risus nec feugiat in fermentum posuere urna nec tincidunt praesent semper feugiat nibh sed pulvinar proin',
                     0);

let post2 = new Post('Et malesuada fames ac turpis',
                     'images/post2.jpg',
                     'Tortor id aliquet lectus proin nibh nisl condimentum id venenatis a condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas sed tempus urna',
                     0);

let post3 = new Post('Non nisi est sit amet',
                     'images/post3.jpg',
                     'Sit amet consectetur adipiscing elit pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas integer eget aliquet nibh praesent tristique magna sit amet purus gravida quis',
                     0);

let post4 = new Post('Sed elementum tempus egestas sed',
                     'images/post4.jpg',
                     'Ut aliquam purus sit amet luctus venenatis lectus magna fringilla urna porttitor rhoncus dolor purus non enim praesent elementum facilisis leo vel fringilla est ullamcorper eget nulla facilisi etiam dignissim',
                     0);

document.addEventListener('DOMContentLoaded', function() {
    let feed = document.getElementById('feed');
    posts.forEach((post, index) => {
        if ((index + 1) % 3 == 0) {
            let adv = new Advertisment();
            adv.render(feed);
        } else {
            post.render(feed);
        }
    });
});