'use strict'
const path = require('path');
module.exports = {
    entry: "./js/src",
    output: {
        filename: "build.js",
        path: path.resolve(__dirname, './js/build'),
    },
    watch: true,
    devtool: "source-map",
    mode: "development"
}