/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}

*/

/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/
const Composition = () => {
    const MakeBackendMagic = () => ({
        MakeBackendMagic: () => console.log('some backend magic...')
    })
    const MakeFrontendMagic = () => ({
        MakeFrontendMagic: () => console.log('some frontend magic...')
    })
    const MakeItLooksBeautiful = () => ({
        MakeItLooksBeautiful: () => console.log('that\'s look beautiful now!')
    })
    const DistributeTasks = () => ({
        DistributeTasks: () => console.log('you! work! now!')
    })
    const DrinkSomeTea = () => ({
        DrinkSomeTea: () => console.log('let\'s drink some tea...')
    })
    const WatchYoutube = () => ({
        WatchYoutube: () => console.log('haha this video is so funny!')
    })
    const Procrastinate = () => ({
        Procrastinate: () => console.log('zzz...')
    })
    const GetPerson = (type, name, gender, age) => {
        return {
            type: type,
            name: name,
            gender: gender,
            age: age
        };
    }
    // ---------------
    function BackendDeveloper(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            MakeBackendMagic(),
            MakeItLooksBeautiful()
        );
    }
    function FrontendDeveloper(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            MakeFrontendMagic(),
            DrinkSomeTea(),
            WatchYoutube()
        )
    }
    function Designer(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            MakeItLooksBeautiful(),
            WatchYoutube(),
            Procrastinate()
        )
    }
    function ProjectManager(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            DistributeTasks(),
            Procrastinate(),
            DrinkSomeTea()
        )
    }

    // var csharpDeveloper = BackendDeveloper('Tripp Lancaster', 'male', 25);
    // console.log('BackendDeveloper', csharpDeveloper);

    // var javascriptDeveloper = FrontendDeveloper('Maddie Riesman', 'female', 24);
    // console.log('FrontendDeveloper', javascriptDeveloper);

    // var photoshoper = Designer('Blayne Schervish', 'male', 23);
    // console.log('Designer', photoshoper);

    // var projectManager = ProjectManager('Wells Salsberg', 'male', 25);
    // console.log('ProjectManager', projectManager);

    class HeadHunt {
        create(type, name, gender, age) {
            let obj;
            switch (type) {
                case 'backend':
                    obj = BackendDeveloper(type, name, gender, age);
                    break;
                case 'frontend':
                    obj = FrontendDeveloper(type, name, gender, age);
                    break;
                case 'design':
                    obj = Designer(type, name, gender, age);
                    break;
                case 'project':
                    obj = ProjectManager(type, name, gender, age);
            }
            return obj;
        }
    }

    document.addEventListener('DOMContentLoaded', function () {
        fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                let availableEmployyes = document.getElementById('availableEmployees');
                let hiredEmployees = document.getElementById('hiredEmployees');
                let fabric = new HeadHunt();
                json.forEach(function(e) {
                    let employee = fabric.create(e.type, e.name, e.gender, e.age);

                    let tdName = document.createElement('td');
                    tdName.innerHTML = `${employee.name} (${employee.age})`;

                    let tdType = document.createElement('td');
                    tdType.innerHTML = `${employee.type}`;

                    let tdHire = document.createElement('td');
                    let btnHire = document.createElement('button');
                    btnHire.innerHTML = 'Hire';
                    btnHire.addEventListener('click', function() {
                        availableEmployyes.removeChild(tr);

                        let li = document.createElement('li');
                        let span = document.createElement('span');
                        span.innerHTML = `${employee.name} (${employee.type})`;

                        let btnFire = document.createElement('button');
                        btnFire.innerHTML = 'Fire';
                        btnFire.addEventListener('click', function() {
                            hiredEmployees.removeChild(li);
                            availableEmployyes.appendChild(tr);
                        });

                        li.appendChild(span);
                        li.appendChild(btnFire);

                        hiredEmployees.appendChild(li);
                    });
                    tdHire.appendChild(btnHire);

                    let tr = document.createElement('tr');
                    tr.appendChild(tdName);
                    tr.appendChild(tdType);
                    tr.appendChild(tdHire);

                    availableEmployyes.appendChild(tr);
                });
            });
    });
};

export default Composition;