function Diagnostic(phone) {
    phone.status = {};
    for(var prop in phone.details) {
        if (phone.details[prop] === false) {
            phone.status[prop] = false;
        }
    }
    return phone;
}

const RepairScreen = (phone) => { phone.details.screen = true }
const RepairSpeaker = (phone) => { phone.details.speaker = true }
const RepairCamera = (phone) => { phone.details.camera = true }
const RepairConnector = (phone) => { phone.details.connector = true }


function Repair(phone) {
    if(phone.status.screen === false){
        RepairScreen(phone);
    }
    if (phone.status.speaker === false) {
        RepairSpeaker(phone);
    }
    if (phone.status.camera === false) {
        RepairCamera(phone);
    }
    if (phone.status.connector === false) {
        RepairConnector(phone);
    }

    console.log('phone repaired:', phone);
}

function compose(func_a, func_b){
    return function(c){
      return func_a( func_b(c) );
    }
}

export default compose( Repair, Diagnostic );