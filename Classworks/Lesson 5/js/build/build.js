/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./js/src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./js/src/composition.js":
/*!*******************************!*\
  !*** ./js/src/composition.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project', rate: '15/h'}

*/

/*

  Используя методы для создания объектов из задания по композиции написать фабику (HeadHunt), которая будет
  расспределять и создавать сотрудников компании нужного типа.

  Данные для списка сотрудников: http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2

  HeadHunt => {
    hire( obj ){
      ...
    }
  }

  Привязать к интерфейсу и вывести на страницу. На кнопку нанять повесить метод hire из фабрики

*/
const Composition = () => {
    const MakeBackendMagic = () => ({
        MakeBackendMagic: () => console.log('some backend magic...')
    })
    const MakeFrontendMagic = () => ({
        MakeFrontendMagic: () => console.log('some frontend magic...')
    })
    const MakeItLooksBeautiful = () => ({
        MakeItLooksBeautiful: () => console.log('that\'s look beautiful now!')
    })
    const DistributeTasks = () => ({
        DistributeTasks: () => console.log('you! work! now!')
    })
    const DrinkSomeTea = () => ({
        DrinkSomeTea: () => console.log('let\'s drink some tea...')
    })
    const WatchYoutube = () => ({
        WatchYoutube: () => console.log('haha this video is so funny!')
    })
    const Procrastinate = () => ({
        Procrastinate: () => console.log('zzz...')
    })
    const GetPerson = (type, name, gender, age) => {
        return {
            type: type,
            name: name,
            gender: gender,
            age: age
        };
    }
    // ---------------
    function BackendDeveloper(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            MakeBackendMagic(),
            MakeItLooksBeautiful()
        );
    }
    function FrontendDeveloper(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            MakeFrontendMagic(),
            DrinkSomeTea(),
            WatchYoutube()
        )
    }
    function Designer(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            MakeItLooksBeautiful(),
            WatchYoutube(),
            Procrastinate()
        )
    }
    function ProjectManager(name, gender, age) {
        return Object.assign(
            GetPerson(name, gender, age),
            DistributeTasks(),
            Procrastinate(),
            DrinkSomeTea()
        )
    }

    // var csharpDeveloper = BackendDeveloper('Tripp Lancaster', 'male', 25);
    // console.log('BackendDeveloper', csharpDeveloper);

    // var javascriptDeveloper = FrontendDeveloper('Maddie Riesman', 'female', 24);
    // console.log('FrontendDeveloper', javascriptDeveloper);

    // var photoshoper = Designer('Blayne Schervish', 'male', 23);
    // console.log('Designer', photoshoper);

    // var projectManager = ProjectManager('Wells Salsberg', 'male', 25);
    // console.log('ProjectManager', projectManager);

    class HeadHunt {
        create(type, name, gender, age) {
            let obj;
            switch (type) {
                case 'backend':
                    obj = BackendDeveloper(type, name, gender, age);
                    break;
                case 'frontend':
                    obj = FrontendDeveloper(type, name, gender, age);
                    break;
                case 'design':
                    obj = Designer(type, name, gender, age);
                    break;
                case 'project':
                    obj = ProjectManager(type, name, gender, age);
            }
            return obj;
        }
    }

    document.addEventListener('DOMContentLoaded', function () {
        fetch('http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2')
            .then(function (response) {
                return response.json();
            })
            .then(function (json) {
                let availableEmployyes = document.getElementById('availableEmployees');
                let hiredEmployees = document.getElementById('hiredEmployees');
                let fabric = new HeadHunt();
                json.forEach(function(e) {
                    let employee = fabric.create(e.type, e.name, e.gender, e.age);

                    let tdName = document.createElement('td');
                    tdName.innerHTML = `${employee.name} (${employee.age})`;

                    let tdType = document.createElement('td');
                    tdType.innerHTML = `${employee.type}`;

                    let tdHire = document.createElement('td');
                    let btnHire = document.createElement('button');
                    btnHire.innerHTML = 'Hire';
                    btnHire.addEventListener('click', function() {
                        availableEmployyes.removeChild(tr);

                        let li = document.createElement('li');
                        let span = document.createElement('span');
                        span.innerHTML = `${employee.name} (${employee.type})`;

                        let btnFire = document.createElement('button');
                        btnFire.innerHTML = 'Fire';
                        btnFire.addEventListener('click', function() {
                            hiredEmployees.removeChild(li);
                            availableEmployyes.appendChild(tr);
                        });

                        li.appendChild(span);
                        li.appendChild(btnFire);

                        hiredEmployees.appendChild(li);
                    });
                    tdHire.appendChild(btnHire);

                    let tr = document.createElement('tr');
                    tr.appendChild(tdName);
                    tr.appendChild(tdType);
                    tr.appendChild(tdHire);

                    availableEmployyes.appendChild(tr);
                });
            });
    });
};

/* harmony default export */ __webpack_exports__["default"] = (Composition);

/***/ }),

/***/ "./js/src/hoc.js":
/*!***********************!*\
  !*** ./js/src/hoc.js ***!
  \***********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
function Diagnostic(phone) {
    phone.status = {};
    for(var prop in phone.details) {
        if (phone.details[prop] === false) {
            phone.status[prop] = false;
        }
    }
    return phone;
}

const RepairScreen = (phone) => { phone.details.screen = true }
const RepairSpeaker = (phone) => { phone.details.speaker = true }
const RepairCamera = (phone) => { phone.details.camera = true }
const RepairConnector = (phone) => { phone.details.connector = true }


function Repair(phone) {
    if(phone.status.screen === false){
        RepairScreen(phone);
    }
    if (phone.status.speaker === false) {
        RepairSpeaker(phone);
    }
    if (phone.status.camera === false) {
        RepairCamera(phone);
    }
    if (phone.status.connector === false) {
        RepairConnector(phone);
    }

    console.log('phone repaired:', phone);
}

function compose(func_a, func_b){
    return function(c){
      return func_a( func_b(c) );
    }
}

/* harmony default export */ __webpack_exports__["default"] = (compose( Repair, Diagnostic ));

/***/ }),

/***/ "./js/src/index.js":
/*!*************************!*\
  !*** ./js/src/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _hoc__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./hoc */ "./js/src/hoc.js");
/* harmony import */ var _composition__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./composition */ "./js/src/composition.js");



// let iPhone = {
//     model: 'XS',
//     details: {
//       screen: false,
//       speaker: true,
//       camera: true,
//       connector: false
//     }
//   }

// RepairPhone(iPhone);

Object(_composition__WEBPACK_IMPORTED_MODULE_1__["default"])();

/***/ })

/******/ });
//# sourceMappingURL=build.js.map