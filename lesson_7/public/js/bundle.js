/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/es7_functional_decorator/index.js":
/*!*******************************************************!*\
  !*** ./application/es7_functional_decorator/index.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nfunction _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {\n  var desc = {};\n  Object['ke' + 'ys'](descriptor).forEach(function (key) {\n    desc[key] = descriptor[key];\n  });\n  desc.enumerable = !!desc.enumerable;\n  desc.configurable = !!desc.configurable;\n\n  if ('value' in desc || desc.initializer) {\n    desc.writable = true;\n  }\n\n  desc = decorators.slice().reverse().reduce(function (desc, decorator) {\n    return decorator(target, property, desc) || desc;\n  }, desc);\n\n  if (context && desc.initializer !== void 0) {\n    desc.value = desc.initializer ? desc.initializer.call(context) : void 0;\n    desc.initializer = undefined;\n  }\n\n  if (desc.initializer === void 0) {\n    Object['define' + 'Property'](target, property, desc);\n    desc = null;\n  }\n\n  return desc;\n}\n\nvar FunctionDecorator = exports.FunctionDecorator = function FunctionDecorator() {\n\n  /*\r\n    ES6 Function Decorator:\r\n  */\n  function fluent(fn) {\n    return function () {\n      for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {\n        args[_key] = arguments[_key];\n      }\n\n      console.log('fluent args', args);\n      fn.apply(this, args);\n      return this;\n    };\n  }\n  function Person() {}\n  Person.prototype.setName = function (first, last) {\n    this.first = first;\n    this.last = last;\n  };\n\n  Person.prototype.sayName = function () {\n    console.log('Person name is', this.first, this.last);\n  };\n\n  var Peter = new Person();\n  // Peter.setName('Peter', 'Jackson');\n  // Peter.sayName();\n  // console.log(Peter);\n\n  // Peter.setName('Peter', 'Parker').sayName().setName('Bilbo', 'Bagins').sayName().sayName();\n};\n\nvar es7Decorator = exports.es7Decorator = function es7Decorator() {\n  var _dec, _desc, _value, _class;\n\n  /*\r\n    Декораторы принимают 3 аргумента:\r\n      - target : обьект к которому применяется декоратор\r\n      - key : название метода\r\n      - descriptor : дескриптор метода или свойства обьекта, это обьект который определяет\r\n      как эта функция должна работать.\r\n        Object.defineProperty();\r\n        Благодаря декоратору, мы можем применить, его к ф-и или обьекту и вернуть примененный дескриптор.\r\n      Новый дескриптор который мы возвращаем, станет дескриптором функции к которой мы хотим его применить.\r\n        Звучит немного не очевидно, и сразу не совсем понятно, что мы можем с этим делать.\r\n      На самом деле эта механика очень гибкая. Одно из свойств аргумента descriptor это\r\n      value, который является функцией которую мы хотим присвоить.\r\n      Мы можем перезаписать это новой функцией или в целом делать что угодно с этой функцией.\r\n  */\n  //\n  function fluentDecorator(target, key, descriptor) {\n    var originFn = descriptor.value;\n    descriptor.value = function () {\n      for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {\n        args[_key2] = arguments[_key2];\n      }\n\n      originFn.apply(this, args);\n      return this;\n    };\n  }\n\n  function decorate(target, key, descriptor) {\n    console.log('target', target, 'key', key, 'descriptor', descriptor);\n    var originFn = descriptor.value;\n\n    descriptor.value = function () {\n      for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {\n        args[_key3] = arguments[_key3];\n      }\n\n      originFn.apply(target, args);\n      return target;\n    };\n  }\n\n  var Person = (_dec = DecorateWith(decorate), (_class = function () {\n    function Person() {\n      _classCallCheck(this, Person);\n    }\n\n    _createClass(Person, [{\n      key: 'setName',\n      value: function setName(first, last) {\n        this.first = first;\n        this.last = last;\n      }\n    }, {\n      key: 'sayName',\n      value: function sayName() {\n        console.log('Person name is', this.first, this.last);\n      }\n    }]);\n\n    return Person;\n  }(), (_applyDecoratedDescriptor(_class.prototype, 'setName', [_dec], Object.getOwnPropertyDescriptor(_class.prototype, 'setName'), _class.prototype), _applyDecoratedDescriptor(_class.prototype, 'sayName', [fluentDecorator], Object.getOwnPropertyDescriptor(_class.prototype, 'sayName'), _class.prototype)), _class));\n\n\n  var Hobbit = new Person();\n  Hobbit.setName('Barliman', 'Butterbur').sayName().setName('222', '333').sayName();\n\n  //\n  // Небольшой хак, для динамичного использования декораторов\n  function DecorateWith(decorator) {\n    return function (target, name, descriptor) {\n      descriptor.value = decorator.call(target, descriptor.value);\n    };\n  }\n\n  // - - - - - - - - - - - - - - - - - - - - - -\n\n  // let Arr = Array(100000).fill(0).map( item => Math.floor(Math.random()*100) );\n  // console.log( Arr );\n  // console.log( '- - - - - - - - - - - -');\n  //\n  // function time( target, key, descriptor){\n  //   // Делаем bind на случай если в нашей исходной функии используется this\n  //   const originFn = descriptor.value.bind(this);\n  //\n  //   let index = 0;\n  //   descriptor.value = function ( ...args){\n  //     let id = index++;\n  //     console.time( key + id );\n  //     let value = originFn( ...args );\n  //     console.timeEnd( key + id);\n  //     return value;\n  //   }\n  // }\n  //\n  // const Obj = {\n  //   @time\n  //   squareAll( arr) {\n  //     return arr.map( x => x * x );\n  //   },\n  //   @time\n  //   message(message){\n  //     console.log( message );\n  //   }\n  // }\n  // let x = Obj.squareAll( Arr )\n  // console.log( Arr, x);\n  //\n  // Obj.message('Hello');\n\n}; //end\n\n//# sourceURL=webpack:///./application/es7_functional_decorator/index.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _es7_functional_decorator = __webpack_require__(/*! ./es7_functional_decorator/ */ \"./application/es7_functional_decorator/index.js\");\n\nvar _observer = __webpack_require__(/*! ./observer */ \"./application/observer/index.js\");\n\nvar _observer2 = _interopRequireDefault(_observer);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// import CustomEvents from './observer/CustomEvents';\n// import obs from '../classworks/observer';\n\n// 0. ES7 Decorator\n// FunctionDecorator();\n// es7Decorator();\n// 1. Observer ->\n// Точка входа в наше приложение\nconsole.log(_observer2.default);\n(0, _observer2.default)();\nconsole.log('INDEX');\n// 2. CustomEvents ->\n// CustomEvents();\n// obs();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/observer/Observer.js":
/*!******************************************!*\
  !*** ./application/observer/Observer.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nfunction Observable() {\n  // Создаем список подписаных обьектов\n  var observers = [];\n  // Оповещение всех подписчиков о сообщении\n  this.sendMessage = function (msg) {\n    observers.map(function (obs) {\n      obs.notify(msg);\n    });\n  };\n  // Добавим наблюдателя\n  this.addObserver = function (observer) {\n    observers.push(observer);\n  };\n}\n// Сам наблюдатель:\nfunction Observer(behavior) {\n  // Делаем функцию, что бы через callback можно\n  // было использовать различные функции внутри\n  this.notify = function (callback) {\n    behavior(callback);\n  };\n}\n\nexports.Observable = Observable;\nexports.Observer = Observer;\n\n//# sourceURL=webpack:///./application/observer/Observer.js?");

/***/ }),

/***/ "./application/observer/demo1.js":
/*!***************************************!*\
  !*** ./application/observer/demo1.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _Observer = __webpack_require__(/*! ./Observer */ \"./application/observer/Observer.js\");\n\nvar Demo1 = function Demo1() {\n  console.log('DEMO 1 ONLINE');\n  var observable = new _Observer.Observable();\n  var obs1 = new _Observer.Observer(function (msg) {\n    return console.log(msg);\n  });\n  var obs2 = new _Observer.Observer(function (msg) {\n    return console.warn(msg);\n  });\n  var obs3 = new _Observer.Observer(function (msg) {\n    return console.error(msg);\n  });\n\n  observable.addObserver(obs1);\n  observable.addObserver(obs2);\n  observable.addObserver(obs3);\n\n  console.log(observable);\n\n  //  Проверим абстрактно как оно работает:\n  setTimeout(function () {\n    // оправим сообщение, с текущей датой:\n    observable.sendMessage('Now is' + new Date());\n  }, 2000);\n};\n\nexports.default = Demo1;\n\n//# sourceURL=webpack:///./application/observer/demo1.js?");

/***/ }),

/***/ "./application/observer/demo2.js":
/*!***************************************!*\
  !*** ./application/observer/demo2.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _Observer = __webpack_require__(/*! ./Observer */ \"./application/observer/Observer.js\");\n\nvar Demo2 = function Demo2() {\n\n  /*\r\n    Рассмотрим на примере интернет магазина:\r\n  */\n  console.log('DEMO 2');\n  var Products = [{\n    id: 1,\n    name: 'Samsung Galaxy S8 ',\n    price: 21999,\n    imageLink: 'https://i1.rozetka.ua/goods/1894533/samsung_galaxy_s8_64gb_black_images_1894533385.jpg'\n  }, {\n    id: 2,\n    name: 'Apple AirPort Capsule',\n    price: 10700,\n    imageLink: 'https://i1.rozetka.ua/goods/3330569/apple_a1470_me177_images_3330569615.jpg'\n  }, {\n    id: 3,\n    name: 'Apple iPhone X',\n    price: 35999,\n    imageLink: 'https://i1.rozetka.ua/goods/2433231/apple_iphone_x_64gb_silver_images_2433231297.jpg'\n  }, {\n    id: 4,\n    name: 'LG G6 Black ',\n    price: 15999,\n    imageLink: 'https://i1.rozetka.ua/goods/1892329/copy_lg_lgh845_acistn_58d8fc4a87d51_images_1892329834.jpg'\n  }];\n\n  // Создадим наблюдателя:\n  var observable = new _Observer.Observable();\n  // Трех обсерверов:\n  var basketObs = new _Observer.Observer(function (id) {\n    var filtredToBasket = Products.filter(function (item) {\n      return Number(item.id) === Number(id);\n    });\n    // console.log();\n    Cart.push(filtredToBasket[0]);\n    renderBasket();\n  });\n\n  var serverObs = new _Observer.Observer(function (id) {\n    var filtredToBasket = Products.filter(function (item) {\n      return Number(item.id) === Number(id);\n    });\n    var msg = '\\u0422\\u043E\\u0432\\u0430\\u0440 ' + filtredToBasket[0].name + ' \\u0434\\u043E\\u0431\\u0430\\u0432\\u043B\\u0435\\u043D \\u0432 \\u043A\\u043E\\u0440\\u0437\\u0438\\u043D\\u0443';\n    console.log(msg);\n  });\n\n  var iconObs = new _Observer.Observer(function (id) {\n    var filtredToBasket = Products.filter(function (item) {\n      return Number(item.id) === Number(id);\n    });\n\n    var products__cart = document.getElementById('products__cart');\n    products__cart.innerText = Cart.length;\n  });\n\n  observable.addObserver(basketObs);\n  observable.addObserver(serverObs);\n  observable.addObserver(iconObs);\n\n  // Render Data - - - - - - - - - - - -\n  var Cart = [];\n  var products__row = document.getElementById('products__row');\n\n  function renderBasket() {\n    var cartElem = document.getElementById('cart');\n    var message = void 0;\n    if (Cart.length === 0) {\n      message = 'У вас в корзине пусто';\n    } else {\n      var Sum = Cart.reduce(function (prev, current) {\n        return prev += Number(current.price);\n      }, 0);\n      message = '\\u0423 \\u0432\\u0430\\u0441 \\u0432 \\u043A\\u043E\\u0440\\u0437\\u0438\\u043D\\u0435 ' + Cart.length + ' \\u0442\\u043E\\u0432\\u0430\\u0440\\u043E\\u0432, \\u043D\\u0430 \\u0441\\u0443\\u043C\\u043C\\u0443: ' + Sum + ' \\u0433\\u0440\\u043D.';\n    }\n    cartElem.innerHTML = '<h2>' + message + '</h2><ol></ol>';\n\n    var ol = cartElem.querySelector('ol');\n    Cart.map(function (item) {\n      var li = document.createElement('li');\n      li.innerText = item.name + ' (' + item.price + ' \\u0433\\u0440\\u043D.)';\n      ol.appendChild(li);\n    });\n  }\n\n  Products.map(function (item) {\n    var product = document.createElement('div');\n    product.className = \"product\";\n    product.innerHTML = '<div class=\"product__image\">\\n            <img src=\"' + item.imageLink + '\"/>\\n          </div>\\n          <div class=\"product__name\">' + item.name + '</div>\\n          <div class=\"product__price\">' + item.price + ' \\u0433\\u0440\\u043D.</div>\\n          <div class=\"product__action\">\\n            <button class=\"product__buy\" data-id=' + item.id + '> \\u041A\\u0443\\u043F\\u0438\\u0442\\u044C </button>\\n          </div>';\n    var buyButton = product.querySelector('.product__buy');\n    buyButton.addEventListener('click', function (e) {\n      var id = e.target.dataset.id;\n      observable.sendMessage(id);\n    });\n    products__row.appendChild(product);\n  });\n\n  renderBasket();\n};\n\nexports.default = Demo2;\n\n//# sourceURL=webpack:///./application/observer/demo2.js?");

/***/ }),

/***/ "./application/observer/index.js":
/*!***************************************!*\
  !*** ./application/observer/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _demo = __webpack_require__(/*! ./demo1 */ \"./application/observer/demo1.js\");\n\nvar _demo2 = _interopRequireDefault(_demo);\n\nvar _demo3 = __webpack_require__(/*! ./demo2 */ \"./application/observer/demo2.js\");\n\nvar _demo4 = _interopRequireDefault(_demo3);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\nvar ObserverDemo = function ObserverDemo() {\n  // Abstract Demo 2\n  // Demo1();\n\n  // Functional Demo:\n  // Demo2();\n\n}; //observer Demo\n\nexports.default = ObserverDemo;\n\n//# sourceURL=webpack:///./application/observer/index.js?");

/***/ })

/******/ });