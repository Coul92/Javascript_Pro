// Точка входа в наше приложение
import { FunctionDecorator, es7Decorator} from './es7_functional_decorator/';
import Observer from './observer';
// import CustomEvents from './observer/CustomEvents';
// import obs from '../classworks/observer';

// 0. ES7 Decorator
// FunctionDecorator();
// es7Decorator();
// 1. Observer ->
console.log( Observer );
Observer();
console.log('INDEX');
// 2. CustomEvents ->
// CustomEvents();
// obs();
