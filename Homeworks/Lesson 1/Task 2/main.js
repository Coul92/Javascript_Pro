document.addEventListener('DOMContentLoaded', function () {
    let output = document.getElementById('output');
    const SORT_BY_BALANCE = 'SORT_BY_BALANCE';
    const SORT_BY_EMPLOYEES_COUNT = 'SORT_BY_EMPLOYEES_COUNT';
    const SORT_EMPLOYEES_BY_AGE = 'SORT_EMPLOYEES_BY_AGE';
    const SORT_EMPLOYEES_BY_GENDER = 'SORT_EMPLOYEES_BY_GENDER';
    function loadEmployeesData(employees, company, sortBy) {
        if (sortBy) {
            switch(sortBy) {
                case SORT_EMPLOYEES_BY_AGE:
                employees.sort((a, b) => a.age - b.age);
                break;
                case SORT_EMPLOYEES_BY_GENDER:
                employees.sort((a, b) => a.gender === 'female');
                break;
            }
        }

        output.innerHTML = '';
        let aBack = document.createElement('a');
        aBack.href = '#';
        aBack.innerHTML = '<- Назад к списку компаний';
        aBack.onclick = function(e) {
            e.preventDefault();
            loadData();
        }
        output.appendChild(aBack);
        let spanSeparator = document.createElement('span');
        spanSeparator.innerHTML = '|';
        output.appendChild(spanSeparator);
        let spanCompany = document.createElement('span');
        spanCompany.innerHTML = company;
        output.appendChild(spanCompany);

        let table = document.createElement('table');

        output.appendChild(document.createElement('br'));
        let input = document.createElement('input');
        let button = document.createElement('button');
        button.innerHTML = 'Search';
        button.onclick = function() {
            table.innerHTML = '';
            let filteredEmployees = employees.filter(employee => employee.name.includes(input.value));
            loadEmployeesData(filteredEmployees, company, '');
        }
        output.appendChild(input);
        output.appendChild(button);

        
        table.border = '1';
        let tr = document.createElement('tr');

        let thName = document.createElement('th');
        thName.innerHTML = 'Name';
        let thGender = document.createElement('th');
        let aGender = document.createElement('a');
        aGender.href = '#';
        aGender.innerHTML = 'Gender';
        aGender.onclick = function(e) {
            e.preventDefault();
            loadEmployeesData(employees, company, SORT_EMPLOYEES_BY_GENDER, searchEmployeesBy);
        }
        thGender.appendChild(aGender);
        let thAge = document.createElement('th');
        let aAge = document.createElement('a');
        aAge.href = '#';
        aAge.innerHTML = 'Age';
        aAge.onclick = function(e) {
            e.preventDefault();
            loadEmployeesData(employees, company, SORT_EMPLOYEES_BY_AGE, searchEmployeesBy);
        }

        thAge.appendChild(aAge);
        let thContacts = document.createElement('th');
        thContacts.innerHTML = 'Contacts';

        tr.appendChild(thName);
        tr.appendChild(thGender);
        tr.appendChild(thAge);
        tr.appendChild(thContacts);

        table.appendChild(tr);

        for(let i = 0; i < employees.length; i++) {
            tr = document.createElement('tr');

            let tdName = document.createElement('td');
            tdName.innerHTML = employees[i].name;
            tr.appendChild(tdName);

            let tdGender = document.createElement('td');
            tdGender.innerHTML = employees[i].gender;
            tr.appendChild(tdGender);

            let tdAge = document.createElement('td');
            tdAge.innerHTML = employees[i].age;
            tr.appendChild(tdAge);

            let tdContacts = document.createElement('td');
            tdContacts.innerHTML = `emails:<br>`;
            for(let j = 0; j < employees[i].emails.length; j++) {
                tdContacts.innerHTML += `${employees[i].emails[j]}<br>`;
            }
            tdContacts.innerHTML += `phones:<br>`;
            for(let j = 0; j < employees[i].phones.length; j++) {
                tdContacts.innerHTML += `${employees[i].phones[j]}<br>`;
            }
            tr.appendChild(tdContacts);

            table.appendChild(tr);
        }

        output.appendChild(table);
    }


    function loadData(sortBy) {
        
        output.innerHTML = '';
        let table = document.createElement('table');
        table.border = '1';
        let tr = document.createElement('tr');

        let thBalance = document.createElement('th');
        let aBalance = document.createElement('a');
        aBalance.innerHTML = 'Balance';
        aBalance.href = '#';
        aBalance.onclick = function(e) {
            e.preventDefault();
            loadData(SORT_BY_BALANCE);
        }
        thBalance.appendChild(aBalance);
        let thCompany = document.createElement('th');
        thCompany.innerHTML = 'Company';
        let thRegistered = document.createElement('th');
        thRegistered.innerHTML = 'Registered';
        let thAddress = document.createElement('th');
        thAddress.innerHTML = 'Показать адрес';
        let thEmployeesCount = document.createElement('th');
        let aEmployeesCount = document.createElement('a');
        aEmployeesCount.href = '#';
        aEmployeesCount.innerHTML = 'Количество сотрудников';
        aEmployeesCount.onclick = function(e) {
            e.preventDefault();
            loadData(SORT_BY_EMPLOYEES_COUNT);
        }
        thEmployeesCount.appendChild(aEmployeesCount);
        let thEmployees = document.createElement('th');
        thEmployees.innerHTML = 'Сотрудники';

        tr.appendChild(thBalance);
        tr.appendChild(thCompany);
        tr.appendChild(thRegistered);
        tr.appendChild(thAddress);
        tr.appendChild(thEmployeesCount);
        tr.appendChild(thEmployees);

        table.appendChild(tr);
        output.appendChild(table);

        fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2').then(response => {
            response.json().then(response => {
                console.log(response);

                if (sortBy) {
                    switch(sortBy) {
                        case SORT_BY_BALANCE:
                        response.sort((a, b) => {
                            return Number(a.balance.replace(/^\D+/g, '')) - Number(b.balance.replace(/^\D+/g, ''));
                        });
                        break;
                        case SORT_BY_EMPLOYEES_COUNT:
                        response.sort((a, b) => {
                            return a.employers.length - b.employers.length;
                        });
                        break;
                    }
                    
                }

                for (let i = 0; i < response.length; i++) {
                    let tr = document.createElement('tr');

                    let tdBalance = document.createElement('td');
                    let tdCompany = document.createElement('td');
                    let tdRegistered = document.createElement('td');
                    let tdAddress = document.createElement('td');
                    let aAddress = document.createElement('a');
                    aAddress.href = '#';
                    aAddress.innerText = "Показать адрес";
                    aAddress.onclick = function (e) {
                        e.preventDefault();
                        tdAddress.innerText = `City ${response[i].address.city}\n
                                   Zip ${response[i].address.zip}\n
                                   Country ${response[i].address.country}\n
                                   State ${response[i].address.state}\n
                                   Street ${response[i].address.street}\n
                                   House ${response[i].address.house}`;
                    }
                    let tdEmployeersLength = document.createElement('td');
                    let tdEmployers = document.createElement('td');
                    let aEmployers = document.createElement('a');
                    aEmployers.href = '#';
                    aEmployers.innerHTML = 'Показать сотрудников';
                    aEmployers.onclick = function(e) {
                        e.preventDefault();
                        loadEmployeesData(response[i].employers, response[i].company);
                    }
                    
                    tdBalance.innerHTML = response[i].balance;
                    tdCompany.innerHTML = response[i].company;
                    tdRegistered.innerHTML = response[i].registered;
                    tdAddress.appendChild(aAddress);
                    tdEmployeersLength.innerHTML = response[i].employers.length;
                    tdEmployers.appendChild(aEmployers);

                    tr.appendChild(tdBalance);
                    tr.appendChild(tdCompany);
                    tr.appendChild(tdRegistered);
                    tr.appendChild(tdAddress);
                    tr.appendChild(tdEmployeersLength)
                    tr.appendChild(tdEmployers);

                    table.appendChild(tr);
                }
            });
        });
    }

    loadData();
});