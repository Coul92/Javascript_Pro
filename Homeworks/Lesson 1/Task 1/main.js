document.addEventListener('DOMContentLoaded', function() {
    const ITEA_COURSES = ["Курс HTML & CSS", "JavaScript базовый курс", "JavaScript продвинутый курс", "JavaScript Professional", "Angular 2.4 (базовый)", "Angular 2.4 (продвинутый)", "React.js", "React Native", "Node.js", "Vue.js"];
    let ITEA_COURSES_LENGTHS = ITEA_COURSES.map(item => {
        return {item: item, length: item.length};
    });
    let output = document.getElementById('output');
    let h1 = document.createElement('h1');
    h1.innerText = 'При помощи методов изложеных в arrays.js , переформатировать ITEA_COURSES в массив который содержит длину строк каждого из элементов';
    output.appendChild(h1);
    let ul = document.createElement("ul");
    for(let i = 0; i < ITEA_COURSES_LENGTHS.length; i++) {
        let li = document.createElement('li');
        li.innerText = `${ITEA_COURSES_LENGTHS[i].item} have length: ${ITEA_COURSES_LENGTHS[i].length}`;
        ul.appendChild(li);
    }
    output.appendChild(ul);

    h1 = document.createElement('h1');
    h1.innerText = 'Самостоятельно изучить метод Array.sort. Отфильтровать массив ITEA_COURSES по алфавиту';
    let h2 = document.createElement('h2');
    h2.innerText = '+ Бонусный бал. Вывести на страничку списком';
    output.appendChild(h1);
    output.appendChild(h2);
    ITEA_COURSES.sort();
    ul = document.createElement("ul");
    for(let i = 0; i < ITEA_COURSES.length; i++) {
        let li = document.createElement('li');
        li.innerText = `${ITEA_COURSES[i]}`;
        ul.appendChild(li);
    }
    output.appendChild(ul);

    h1 = document.createElement('h1');
    h1.innerText = 'Реализация функции поиска по массиву ITEA_COURSES';
    h2 = document.createElement('h2');
    h2.innerText = '+ Бонусный бал. Вывести на страничку инпут и кнопку по которой будет срабатывать поиск';
    output.appendChild(h1);
    output.appendChild(h2);
    let input = document.createElement('input');
    let button = document.createElement('button');
    button.innerHTML = 'Поиск';
    let search = document.getElementById('search');
    let searchResult = document.getElementById('searchResult');
    search.appendChild(input);
    search.appendChild(button);
    button.onclick = function() {
        searchResult.innerHTML = '';
        let result = ITEA_COURSES.filter(item => item.includes(input.value));
        for(let i = 0; i < result.length; i++) {
            let li = document.createElement('li');
            li.innerText = `${result[i]}`;
            searchResult.appendChild(li);
        }
    };
});