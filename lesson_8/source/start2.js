const module = () => {
  class Test {
    constructor( name ){
      this.name = name;
    }
  }

  class SuperTest extends Test {
    constructor(props){
      super(props);
    }
  }


}

export default module;
