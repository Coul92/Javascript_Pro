document.addEventListener('DOMContentLoaded', () => {

  let MyTimerProto = Object.create(HTMLElement.prototype);
  MyTimerProto.tick = function() { // свой метод tick
    this.innerHTML++;
  };

  document.registerElement("my-timer", {
    prototype: MyTimerProto
  });

  setInterval(function() {
    timer.tick();
  }, 1000);

});
