document.addEventListener('DOMContentLoaded', () => {
    /*
      Life Cycle:
      createdCallback	Элемент создан
      attachedCallback	Элемент добавлен в документ
      detachedCallback	Элемент удалён из документа
      attributeChangedCallback(name, prevValue, newValue)	Атрибут добавлен, изменён или удалён
    */

    let youtubeProto = Object.create(HTMLElement.prototype);
    youtubeProto.createdCallback = function(){
      this.link = this.getAttribute('link');
      console.log('yt-player created!', this.link);
    }
    youtubeProto.attachedCallback = function(){
      this.innerHTML = `
        <iframe
        width="560"
        height="315"
        src="https://www.youtube.com/embed/${this.link}"
        frameborder="0"
        allow="autoplay; encrypted-media"
        allowfullscreen></iframe>
      `;
      console.log('yt-player appended to DOM!');
    }
    youtubeProto.detachedCallback = () => {
      console.log('yt-player removed');
    }
    youtubeProto.attributeChangedCallback = function(name, prevValue, newValue){
      console.log( name, prevValue, newValue, this );
      if( name === 'link' && prevValue !== newValue){
        this.querySelector('iframe').src = `https://www.youtube.com/embed/${newValue}`;

      }
    }
    
    document.registerElement("yt-player", {
      prototype: youtubeProto
    });
});
