
	/*
		Задание 1
		
		Установка пакетов через npm и запуск тестового приложения. 
		
		1.  Установите Node.js под вашу систему с офф. сайта: https://nodejs.org/uk/
			Если node у вас уже установлен этот пункт можете пропустить.
			Проверить наличие node.js у вас в системе можно проверить командой node -v / npm -v
	
		2. 	Инициализируйте пустой проект в вашей рабочей директории
		3.	Через npm или yarn установите следующий список зависимостей:
			
			- express
			- gulp
			- normalize.css
			- jQuery
			
		4. 	Через командную строку запустите сервер express используя Hello-world заготовку:
			http://expressjs.com/en/starter/hello-world.html
			node yourscriptname.js
		
	*/
	
	/*
		Telegramm link: https://goo.gl/7bFZjN
	*/
	
	/*
	
		Задание 2.
		
		Проверка уровня знаний для старта.
		
		Имеется обьект:
		
		[
			{
				link: "#1",
				name: "Established fact",
				description: "The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. ",
				image: "http://telegram.org.ru/uploads/posts/2017-10/1507400926_file_162303.jpg"
			},
			{
				link: "#2",
				name: "Many packages",
				description: "Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.",
				image: "http://telegram.org.ru/uploads/posts/2017-10/1507400859_file_162309.jpg"
			},
			{
				link: "#3",
				name: "Suffered alteration",
				description: "It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged",
			
				description: "Looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature.",	image: "http://telegram.org.ru/uploads/posts/2017-10/1507400896_file_162315.jpg"
			},{
				link: "#4",
				name: "Discovered source",
				image: "http://telegram.org.ru/uploads/posts/2017-10/1507400878_file_162324.jpg"
			},{
				link: "#5",
				name: "Handful model",
				description: "The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.",
				image: "http://telegram.org.ru/uploads/posts/2017-10/1507400876_file_162328.jpg"
			},
			
			
		];		
	*/	
	