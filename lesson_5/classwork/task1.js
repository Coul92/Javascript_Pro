/*

  HOC.
  Задание написать HOC RepairPhone, которая будет принимать 2 функции.
  1. Diagnostic - которая определяет какой компонент телефона исправен, а какой нет
  2. Repair_Screen, Repair_Speeker - которые будут изменять статус той детали
  за ремонт которой отвечают.
  3. HOC должен выполнить вначале диагностику, а затем ремонт нужной части.

  {
    model: '8',
    status: {
      screen: true,
      speeker: true,
      camera_front: true,
      camera_back: true,
      connector: false
    }
  }


  RepairPhone( phoneObj );
*/
