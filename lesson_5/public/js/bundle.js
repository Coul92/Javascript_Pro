/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/composition_vs_inheritance.js":
/*!***************************************************!*\
  !*** ./application/composition_vs_inheritance.js ***!
  \***************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/*\n\n  Composition vs Inheritance\n  Компоновка (Композиция) против Наследования.\n\n  Наследование, определяет обьект по тому чем он ЯВЛЯЕТСЯ.\n  Композиция, определяет обьект по тому, что он ДЕЛАЕТ\n\n  Опишем проблематику. Слайды!\n\n*/\n// Our Functions\n\nconst Composition = () => {\n\nconst Drive = ( state ) => ({\n  drive: () => console.log('Wroooom!, It\\'s a car ' + state.name )\n});\n\nconst ChangeName = state => ({\n  changeName: ( name ) => {\n    console.log(`Old name:`, state.name, state );\n    state.name = name;\n    console.log(`New name:`, state.name, state );\n  }\n});\n\nconst Refill = ( state ) => ({\n  refill: () => console.log( state.name + ' was refiled')\n});\n\nconst Move = ( state ) => ({\n  move: ( speed ) => {\n    console.log(speed);\n    state.speed += speed ;\n    console.log( state.name + ' is moving. Speed ->' + state.speed );\n  }\n});\n\nconst Fly = ( state ) => ({\n  fly: () => {\n    console.log( state );\n    // state.name = \"qOp\";\n    console.log( state.name + ' flying into sky! Weather is ' + state.weather );\n  }\n});\n\nconst LoggerIn = obj => ({\n  logger: () => {\n    console.log( obj );\n  }\n});\n\n// Проверим ф-ю\nRefill({name: \"Volkswagen\"}).refill(); // Volkswagen was refiled\n//\n// // Наш конструктор.\nconst EcoRefillDrone = (name, speed) => {\n  let state = {\n    name,\n    speed: Number(speed),\n    weather: 'rainy'\n  };\n  return Object.assign(\n    {},\n    state,\n    Drive(state),\n    Refill(state),\n    ChangeName(state),\n    Fly(state),\n    Move(state),\n    LoggerIn(state)\n  );\n};\n\n  const myDrone = EcoRefillDrone('JS-Magic', 100);\n        myDrone.drive();\n        myDrone.refill();\n        myDrone.fly();\n        myDrone.move(100);\n        myDrone.move(100);\n\n        // console.log( 'myDrone', myDrone );\n        // myDrone.logger();\n  //\n  // const myDrone2 = EcoRefillDrone('JS-Is-Amaizing', 100);\n  // myDrone2.move(100);\n  // myDrone2.move(200);\n  // myDrone2.changeName('Dex3');\n        // myDrone2.changeName('HDMI');\n  //\n  // const Logger = obj => console.log( obj );\n  //       Logger(myDrone2);\n  //       myDrone2.logger();\n  //\n  //\n  //       let bindedMove = myDrone2.move.bind(null, 200);\n  //       MoveId.addEventListener('click', bindedMove );\n\n}\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Composition);\n\n\n//# sourceURL=webpack:///./application/composition_vs_inheritance.js?");

/***/ }),

/***/ "./application/fabric.js":
/*!*******************************!*\
  !*** ./application/fabric.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n  /*\n\n      Сегодня разберем некоторые паттерны проектирования JS приложений.\n\n      Разберем паттерн \"Фабрика\" и то как он работает в JS.\n      И какие проблемы он решает.\n\n  */\n  const Fabric = () => {\n\n    class Employee {\n      create( type ){\n        let obj;\n        if( type === 'fulltime'){\n          obj = new Fulltime();\n        }\n        if( type === 'parttime'){\n          obj = new PartTime();\n        }\n        if( type === 'hours'){\n          obj = new Hours();\n        }\n        obj.say = function(){\n          console.log('My type is:')\n        }\n        return obj;\n      }\n    }\n\n    class Fulltime {}\n    class PartTime {}\n    class Hours {}\n\n    let EmpFabric = new Employee();\n\n\n    let emp1 = EmpFabric.create('fulltime');\n    let emp2 = EmpFabric.create('parttime');\n    console.log( emp1, emp2 );\n\n\n\n    class SpaceShipClass {\n      constructor( name, position ){\n        this.name = name;\n        this.position = position;\n\n        this.showYourPosition = this.showYourPosition.bind(this);\n      }\n      flightTo( targetPlanet ){\n        let FlightSpeed = '2000km/s';\n        let { name } = this;\n        console.log( `Ship ${name} flies to ${targetPlanet} at a speed of ` + FlightSpeed );\n      }\n      showYourPosition(){\n        let {name, position} = this;\n        console.log( this );\n        console.log(`My name ${name}. Our position is ${ position[0] }, ${ position[1] }`);\n      }\n    }\n\n    let myClassShip = new SpaceShipClass('X-wing', ['0', '25']);\n        myClassShip.flightTo('Tattoin');\n\n        // проверим что код корректно отработал.\n        console.log( myClassShip );\n\n        let ShootButton = document.getElementById('shotWithClass');\n        // 1. Попробуем повесить метод\n        // ShootButton.addEventListener('click',  myClassShip.showYourPosition);\n        // Программа не работает - this ссылается на элемемнт на который мы кликнули\n\n        // 2. Попробуем указать на обьект через bind(obj)\n        // ShootButton.addEventListener('click',  myClassShip.showYourPosition.bind(myClassShip));\n        // Такой вариант работает, но выглядит уныло :(\n\n        // 3. Забиндим обработчику обьект в конструкторе\n        // ShootButton.addEventListener('click',  myClassShip.showYourPosition);\n        // Работает, но тоже не выглядит как best practice\n\n        // 4. Arrow Functions\n        // ShootButton.addEventListener('click', () => myClassShip.showYourPosition() );\n        // Тоже вариант решения, так как стрелочные функции не переопределяют контекст this\n\n        /*\n\n          Проблема решаема, но возникает на пустом месте.\n          Давайте рассмотрим другой архитектурный подход.\n          Фабрика - это простая функция, которая порождает новый обьект.\n\n        */\n\n\n\n    const SpaceShip = ( name, position ) => {\n      // Приватнное свойсвто!\n      let FlightSpeed = '2000km/s';\n\n      const doSomePrivateStuff = () => 'SectetToken';\n      function decryptSecretToken( SecretToken ){\n        console.log('Our user is:', SecretToken );\n        return {\n          user: 'SuperAdminUser',\n          emain: 'Super@props.ua'\n        };\n      }\n\n      let SecretToken = doSomePrivateStuff();\n      let Comander = decryptSecretToken( SecretToken );\n\n      const moveTo = () => ({\n        changePosition: ( newPosition ) => {\n            undefined.position = newPosition;\n            console.log('wut', undefined.position);\n        }\n      });\n\n      return Object.assign(\n        {},\n        moveTo(),\n        {\n          name: name,\n          position: position,\n          commander: Comander,\n          // Add capitan form _.js\n          flightTo: ( targetPlanet ) => console.log( `Ship ${name} flies to ${targetPlanet} at a speed of ` + FlightSpeed ),\n          showYourPosition: () => {\n            // Можно использовать как обычные, так и стрелочные функции\n            // Не нужно использовать деконстркуцию, обращаемся напрямую к свойствам\n            // Не нужно использовать this что бы обратиться к свойствам\n            console.log(`My name ${name}. Base speed: ${FlightSpeed} Our position is ${ position[0] }, ${ position[1] }`);\n          }\n        }\n      );\n    };\n    // Проверим работу\n    const myShip = SpaceShip(\"X-wing\", [\"25\", \"35\"]);\n          myShip.flightTo('Earth');\n\n    // console.log( myShip );\n    //\n    // const myShip2 = SpaceShip(\"Millennium Falcon\", [\"1\", \"0\"]);\n          // myShip2.flightTo('Endor');\n          // myShip2.showYourPosition();\n          // myShip2.changePosition(['44','91']);\n          // console.log( myShip2 );\n    // // Проверим работу класса вызвав публичное свойство name\n    // // Проверим работу обработчика\n    // ShootButton.addEventListener('click', myShip.showYourPosition);\n    // Done.\n\n    /*\n\n      Итог - фабрика очень полезный паттерн проектирования, который:\n\n      1. + Является простой функцией которая создает обьекты\n      2. + Решает проблемы с контекстом выполнения методов обьекта\n      3. + Позволяет использовать приватные функции и свойста\n      4. + Является полноценной заменой классу\n\n      5. - Является более медленной заменой класам. По причине того, что\n      все переменные и методы инициализируются каждый раз при вызове фабрики.\n      Минус очень абстрактный, так как пример выше с использованием фабрики\n      генерируется за 0.00004 ms, обьект созданый через класс генерируется за\n      0.00002 ms - т.е в 2 раза быстрее.\n      Эффект такого может быть заметен если вы допустим заходите сгенерировать\n      10000 новых обьектов через фабрику. Выполнение такого скрипта займет\n      2ms через класс и 4ms черещ фабрику соответствено.\n      Другой вопрос, в том, зачем нужно генерировать за 1 итерацию 10000\n      обьектов ¯\\_(ツ)_/¯\n\n    */\n  }\n\n\n  /* harmony default export */ __webpack_exports__[\"default\"] = (Fabric);\n\n\n//# sourceURL=webpack:///./application/fabric.js?");

/***/ }),

/***/ "./application/hoc.js":
/*!****************************!*\
  !*** ./application/hoc.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n\n/*\n\n  Higher Order Functions\n  function is a values\n\n*/\nlet hofDemo = () => {\n\n  let multiply = x => x * x;\n  let nine = multiply(3);\n  console.log( 'multiply:', nine );\n\n  /*\n\n    Array.filter (так же как map, forEach, etc...) пример использования HOF в нативном js\n    Паттерн позворялет использовать композцицию что бы собрать из маленьких функций одну большую\n\n  */\n\n  let zoo = [\n    {id:0, name:\"WoofMaker\", species: 'dog'},\n    {id:1, name:\"WhiteFurr\", species: 'rabbit'},\n    {id:2, name:\"MeowMaker\", species: 'cat'},\n    {id:3, name:\"PoopMaker\", species: 'dog'},\n    {id:4, name:\"ScratchMaker\", species: 'cat'},\n  ]\n\n  let isDog = animal => animal.species === 'dog';\n  let isCat = animal => animal.species === 'cat';\n\n  let dogs = zoo.filter( isDog );\n  let cats = zoo.filter( isCat );\n\n  console.log('Here dogs:', dogs);\n  console.log('Here cats:', cats);\n\n  // - - - - - - - - - - - - - - - - - -\n\n  function compose(func_a, func_b){\n    return function(c){\n      return func_a( func_b(c) );\n    }\n  }\n  const addTwo = value => {\n    console.log('Add', value);\n    return value + 2\n  }\n  const multiplyTwo = value => {\n    console.log('Mulitple', value);\n    return value * 2;\n  }\n\n  const addTwoAndMultiplayTwo = compose( addTwo, multiplyTwo );\n  // addTwoAndMultiplayTwo( 10 )\n\n  /*\n    В данном случае происходит следующее:\n    - Вызывается ф-я compose которая принимает ф-и addTwo, multiplyTwo как аргументы\n    - Вызывается функция которая передана как аргумент func_b\n    - Результат её выполнения передается в функция func_a\n    - Общий результат возвращается в ф-ю которая нам возвращается в переменную\n  */\n\n  // console.log(\n  //   addTwoAndMultiplayTwo(2),\n  //   addTwoAndMultiplayTwo(6),\n  //   addTwoAndMultiplayTwo(40)\n  // );\n\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (hofDemo);\n\n\n//# sourceURL=webpack:///./application/hoc.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _composition_vs_inheritance__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./composition_vs_inheritance */ \"./application/composition_vs_inheritance.js\");\n/* harmony import */ var _hoc__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hoc */ \"./application/hoc.js\");\n/* harmony import */ var _fabric__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fabric */ \"./application/fabric.js\");\n\n\n\n\n// comp();\n\n// hoc();\n\nObject(_fabric__WEBPACK_IMPORTED_MODULE_2__[\"default\"])();\n\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ })

/******/ });