import Decorator from './decorator';
import AssignAndRes from './features/object.assign';
import Mediator from './mediator';
import ObjectDefineDemo from './features/define_property'

// import Work1 from '../classworks/mediator';

/*
  1. Decorator as Design Pattern -> ./decorator/index.js
  2. Higher Order Functions -> ./features/hoc.js
  3. Object.defineProperty(...) -> ./features/defineProperty
  4. Object.assign and Spread Operator
  5. Mediator Pattern -> ./mediator
*/

  // Decorator();
  AssignAndRes();
  // ObjectDefineDemo();
  // Mediator();
  // classworks ->
  // Work1();
