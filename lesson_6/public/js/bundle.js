/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/decorator/basicUsage.js":
/*!*********************************************!*\
  !*** ./application/decorator/basicUsage.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\nvar BaseDemo = function BaseDemo() {\n\n  console.log('DECORATOR BASE!');\n\n  function Human(name) {\n    this.name = name;\n    this.currentTemperature = 0;\n    this.minTemperature = -10;\n\n    console.log('new Human ' + this.name + ' arrived!');\n  }\n\n  Human.prototype.ChangeTemperature = function ChangeTemperature(changeValue) {\n    console.log('current', this.currentTemperature + changeValue, 'min', this.minTemperature);\n\n    this.currentTemperature = this.currentTemperature + changeValue;\n\n    if (this.currentTemperature < this.minTemperature) {\n      console.error('Temperature is to low: ' + this.currentTemperature + '. ' + this.name + ' died :(');\n    } else {\n      console.log('It\\'s cold outside (' + this.currentTemperature + ' deg), please wear some clothes, or ' + this.name + ' will die!');\n    }\n  };\n\n  var Morgan = new Human('Morgan');\n  Morgan.ChangeTemperature(-5);\n  Morgan.ChangeTemperature(-6);\n\n  function DressedHuman(Human) {\n    this.name = Human.name;\n    this.clothes = [{ name: 'jacket', temperatureResistance: 20 }, { name: 'hat', temperatureResistance: 5 }, { name: 'scarf', temperatureResistance: 10 }];\n    this.currentTemperature = 0;\n    this.minTemperature = Human.minTemperature - this.clothes.reduce(function (currentResistance, clothe) {\n      console.log('currentResistance', currentResistance, 'clothe', clothe);\n      return currentResistance + clothe.temperatureResistance;\n    }, 0);\n    console.log('new Human ' + this.name + ' arrived! He can survive in temperature ' + this.minTemperature, this);\n  }\n  DressedHuman.prototype = Human.prototype;\n  //\n  var Dexter = new DressedHuman(new Human('Dexter'));\n  Dexter.ChangeTemperature(-6);\n  Dexter.ChangeTemperature(-16);\n  Dexter.ChangeTemperature(-16);\n  Dexter.ChangeTemperature(-26);\n};\n\nexports.default = BaseDemo;\n\n//# sourceURL=webpack:///./application/decorator/basicUsage.js?");

/***/ }),

/***/ "./application/decorator/basicUsage_es6.js":
/*!*************************************************!*\
  !*** ./application/decorator/basicUsage_es6.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError(\"this hasn't been initialised - super() hasn't been called\"); } return call && (typeof call === \"object\" || typeof call === \"function\") ? call : self; }\n\nfunction _inherits(subClass, superClass) { if (typeof superClass !== \"function\" && superClass !== null) { throw new TypeError(\"Super expression must either be null or a function, not \" + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar DecoratorClass = function DecoratorClass() {\n\n  console.log('DECORATOR ON CLASSES!');\n\n  var Human = function () {\n    function Human(name) {\n      _classCallCheck(this, Human);\n\n      this.name = name;\n      this.currentTemperature = 0;\n      this.minTemperature = -10;\n\n      console.log('new Human ' + this.name + ' arrived!', this);\n    }\n\n    _createClass(Human, [{\n      key: 'changeTemperature',\n      value: function changeTemperature(changeValue) {\n        console.log('current', this.currentTemperature + changeValue, 'min', this.minTemperature);\n        var prevTemperature = this.currentTemperature;\n        this.currentTemperature = this.currentTemperature + changeValue;\n\n        if (this.currentTemperature < this.minTemperature) {\n          console.error('Temperature is to low: ' + this.currentTemperature + '. ' + this.name + ' died :(');\n        } else {\n          if (this.currentTemperature > prevTemperature) {\n            console.log('Temperature is growing. Seems someone go to Odessa or drink some hot tea?');\n          } else {\n            console.log('It\\'s cold outside (' + this.currentTemperature + ' deg), please wear some clothes, or ' + this.name + ' will die!');\n          }\n        }\n      }\n    }]);\n\n    return Human;\n  }();\n\n  var Debra = new Human('Debra');\n  Debra.changeTemperature(-5);\n  Debra.changeTemperature(6);\n\n  var DressedHuman = function (_Human) {\n    _inherits(DressedHuman, _Human);\n\n    function DressedHuman(name) {\n      _classCallCheck(this, DressedHuman);\n\n      var _this = _possibleConstructorReturn(this, (DressedHuman.__proto__ || Object.getPrototypeOf(DressedHuman)).call(this, name));\n\n      _this.clothes = [{ name: 'jacket', temperatureResistance: 20 }, { name: 'hat', temperatureResistance: 5 }, { name: 'scarf', temperatureResistance: 10 }];\n      _this.minTemperature = _this.minTemperature - _this.clothes.reduce(function (currentResistance, clothe) {\n        console.log('currentResistance', currentResistance, 'clothe', clothe);\n        return currentResistance + clothe.temperatureResistance;\n      }, 0);\n      console.log('Dressed Human ' + name, _this);\n      return _this;\n    }\n\n    return DressedHuman;\n  }(Human);\n\n  var Masuka = new DressedHuman('Masuka');\n  Masuka.changeTemperature(-25);\n  Masuka.changeTemperature(-26);\n};\n\nexports.default = DecoratorClass;\n\n//# sourceURL=webpack:///./application/decorator/basicUsage_es6.js?");

/***/ }),

/***/ "./application/decorator/index.js":
/*!****************************************!*\
  !*** ./application/decorator/index.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _basicUsage = __webpack_require__(/*! ./basicUsage */ \"./application/decorator/basicUsage.js\");\n\nvar _basicUsage2 = _interopRequireDefault(_basicUsage);\n\nvar _basicUsage_es = __webpack_require__(/*! ./basicUsage_es6 */ \"./application/decorator/basicUsage_es6.js\");\n\nvar _basicUsage_es2 = _interopRequireDefault(_basicUsage_es);\n\nvar _define_property = __webpack_require__(/*! ../features/define_property */ \"./application/features/define_property.js\");\n\nvar _define_property2 = _interopRequireDefault(_define_property);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n/*\r\n  Декоратор — это структурный паттерн проектирования,\r\n  который позволяет динамически добавлять объектам новую\r\n  функциональность, оборачивая их в полезные «обёртки».\r\n\r\n  https://refactoring.guru/ru/design-patterns/decorator\r\n*/\n\nvar DecoratorDemo = function DecoratorDemo() {\n\n  // console.log( 'DECORATOR AS DESIGN PATTERN DEMO!');\n  // Base();\n  // BaseClass();\n  // console.log( '- - - - - - - - - - - -');\n  // Hoc();\n  // Define();\n};\n\nexports.default = DecoratorDemo;\n\n//# sourceURL=webpack:///./application/decorator/index.js?");

/***/ }),

/***/ "./application/features/define_property.js":
/*!*************************************************!*\
  !*** ./application/features/define_property.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar ObjectDefineDemo = function ObjectDefineDemo() {\n\n  /*\r\n    Синтаксис:\r\n    Object.defineProperty(obj, prop, descriptor)\r\n      obj - Объект, в котором объявляется свойство.\r\n    prop - Имя свойства, которое нужно объявить или модифицировать.\r\n    descriptor - Дескриптор – объект, который описывает поведение свойства\r\n      В нём могут быть следующие поля:\r\n    */\n\n  var Test = function () {\n    function Test(name) {\n      _classCallCheck(this, Test);\n\n      this._name = name;\n    }\n\n    _createClass(Test, [{\n      key: 'name',\n      get: function get() {\n        return this._name;\n      }\n    }]);\n\n    return Test;\n  }();\n\n  var test = new Test('test');\n  console.log(test.name);\n\n  // let MyObj = {};\n  //     MyObj.title = \"Prop\";\n  //     MyObj.func = function(){\n  //       return this.name;\n  //     };\n\n  // Object.defineProperty(\n  //   MyObj,\n  //   \"name\",\n  //   {\n  //     // value – значение свойства, по умолчанию undefined\n  //     value: \"Вася\",\n  //     // configurable – если true, то свойство можно удалять, а также менять его\n  //     // в дальнейшем при помощи новых вызовов defineProperty. По умолчанию false.\n  //     configurable: false,\n  //     // writable – значение свойства можно менять, если true. По умолчанию false.\n  //     writable: false,\n  //     //enumerable – если true, то свойство просматривается в цикле for..in и\n  //     // методе Object.keys(). По умолчанию false.\n  //     enumerable: true,\n  //     // get – функция, которая возвращает значение свойства. По умолчанию undefined.\n  //     // get: () => { console.log( 'getter'); },\n  //     // set – функция, которая записывает значение свойства. По умолчанию undefined.\n  //     // set: () => { console.log( 'setter'); }\n  //   });\n\n\n  //writable demo ->\n  // MyObj.name = 10;\n  // MyObj.name\n\n  // configurable demo ->\n  // delete MyObj.name;\n\n  // enumerable demo ->\n  // Object.defineProperty(MyObj, \"func\", {enumerable: false}); // Можно добавить уже после обьявления\n  // for(var key in MyObj) console.log(key);\n  // console.log( Object.keys(MyObj) );\n  // - - - - - - - - - - - - - -\n  // Object.defineProperty(MyObj, \"fullName\", {\n  //   get: function() {\n  //     return `${this.title} ${this.name}`;\n  //   }\n  // });\n\n  // console.log('FullName getter:', MyObj.fullName );\n\n  // Object.defineProperty(MyObj, \"height\", {\n  //   get: function() {\n  //     return `${this.ObjHeight} cm`;\n  //   },\n  //   set: function( value ){\n  //     this.ObjHeight = value;\n  //     console.log( 'ObjHeight setter:', this.ObjHeight );\n  //   }\n  // });\n  //\n  // MyObj.height = 20;\n  // console.log( MyObj.height );\n  // console.log( MyObj );\n};\n\nexports.default = ObjectDefineDemo;\n\n//# sourceURL=webpack:///./application/features/define_property.js?");

/***/ }),

/***/ "./application/features/object.assign.js":
/*!***********************************************!*\
  !*** ./application/features/object.assign.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n/*\r\n  Object Assign, Spread and Rest Operator\r\n  Docs:\r\n    https://developer.mozilla.org/uk/docs/Web/JavaScript/Reference/Global_Objects/Object/assign\r\n*/\n\nvar SpreadOperatorDemo = function SpreadOperatorDemo() {}\n\n/*\r\n  String Literal:\r\n*/\n\n// var a = 'Карп',\n//     b = \"Селедка\";\n// var newStringLiteral = `${a} и ${b} сидели на трубe`;\n//     console.log('newStringLiteral', newStringLiteral);\n//\n// var d = ['Лебедь','Рак','Щука'];\n// var c = `\n// <div>\n//   <ul>\n//     ${\n//       d.map( (item) => `<li>${item}</li>` )\n//     }\n//   </ul>\n// </div>\n// `;\n//\n// console.log(c);\n\n/*\r\n  Object.assign\r\n  syntax: Object.assign(target, ...sources)\r\n*/\n// не коментить DataObj\n// let DataObj = {\n//   data1: 'data1',\n//   data2: 'data2'\n// };\n\n// let DataObj2 = {\n//   data3: 'data3',\n//   data4: 'data4'\n// };\n\n// - - - - - - - -\n\n// let firstAssign = Object.assign(DataObj, DataObj2);\n// console.log('firstAssign', firstAssign);\n// console.log('DataObj', DataObj);\n// Изменяем значение исходного обьекта и проверяем значения обеих\n// DataObj.data5 = 'data5';\n// console.log('DataObj', DataObj);\n// console.log('firstAssign', firstAssign);\n\n// - - - - - - - -\n\n// IMMUTABLE ASSIGN\n// let secondAssign = Object.assign({}, DataObj, DataObj2 );\n// // console.log( 'secondAssign', secondAssign );\n// DataObj.data6 = 'data6';\n// console.log('DataObj - secondAssign', DataObj);\n// console.log('secondAssign', secondAssign);\n\n// - - - - - - - -\n\n// let FunctionalObj = {\n//   x: () => {\n//     console.log('some important stuff');\n//   },\n//   y: {\n//     a: 'a',\n//     b: 'b',\n//     c: 'c'\n//   }\n// };\n// //\n// FunctionalObj.x();\n// let thirdAssign = Object.assign({}, FunctionalObj);\n// console.log( thirdAssign );\n// thirdAssign.x();\n\n// - - - - - - - -\n// convert to obj\n// var v1 = 'abcfadfsdfsds';\n// var v2 = true;\n// var v3 = 10;\n// var v4 = { value : true };\n//\n// var obj = Object.assign(\n//   {},\n//   // v1,   // разберет посимвольно индекс-буква\n//   null, // -> ignore\n//   v2,   // -> ignore\n//   undefined,  // -> ignore\n//   v3,   // -> ignore\n//   v4    // внесет в обьект\n// );\n// console.log(obj);\n\n// - - - - - - - -\n\n// var obj = {\n//   foo: 1,\n//   get bar() {\n//     return 2;\n//   }\n// };\n\n// obj.value = '';\n// // // При попытке скопировать значение с геттером, получим только его value\n// var copy = Object.assign({}, obj);\n// console.log(copy);\n\n\n/*\r\n  REST and Spread Operator\r\n*/\n\n// in Function ->\n// function RestTest(a, b, ...props){\n//   console.log('a:', a, 'b', b, 'props', props);\n//   props.map( item => console.log( 'map rest props', item ) )\n// }\n// var args = [0, 1, 2, 4, 5];\n// RestTest.apply(null, args);\n// RestTest(6,7,8,9,0,'f')\n\n// In array:\n// var iterableObj = ['i','t','e'];\n// var iterableObj2 = ['d','d','a'];\n// var x = [ '4', 'five', 6, ...iterableObj, ...iterableObj2];\n// console.log( 'rest in array:', x);\n\n// concat arrays: old way;\n// var arr1 = [0, 1, 2];\n// var arr2 = [3, 4, 5];\n// Append all items from arr2 onto arr1\n// arr1 = arr1.concat(arr2);\n// new way:\n// arr1 = [...arr1, ...arr2];\n// console.log( arr1 );\n\n// // In obj\n// var obj = { value: 1};\n\n// let objClone = { ...obj, ...DataObj };\n// console.log( 'objClone', objClone );\n\n// let { x, k, ...z } = { x: ['x'], k: ['k'], a: 3, b: 4 };\n// console.log(x); // ['x']\n// console.log(k); // ['k']\n// console.log(z); // { a: 3, b: 4 }\n\n// let data = {\n//   loaded: true,\n//   loading: false,\n//   data: {\n//     value: 1,\n//     value2: 2\n//   }\n// }\n//\n// let x = {\n//   ...data,\n//   value: 2\n// }\n\n// console.log( x);\n\n// end\n\n;exports.default = SpreadOperatorDemo;\n\n//# sourceURL=webpack:///./application/features/object.assign.js?");

/***/ }),

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nvar _decorator = __webpack_require__(/*! ./decorator */ \"./application/decorator/index.js\");\n\nvar _decorator2 = _interopRequireDefault(_decorator);\n\nvar _object = __webpack_require__(/*! ./features/object.assign */ \"./application/features/object.assign.js\");\n\nvar _object2 = _interopRequireDefault(_object);\n\nvar _mediator = __webpack_require__(/*! ./mediator */ \"./application/mediator/index.js\");\n\nvar _mediator2 = _interopRequireDefault(_mediator);\n\nvar _define_property = __webpack_require__(/*! ./features/define_property */ \"./application/features/define_property.js\");\n\nvar _define_property2 = _interopRequireDefault(_define_property);\n\nfunction _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }\n\n// import Work1 from '../classworks/mediator';\n\n/*\r\n  1. Decorator as Design Pattern -> ./decorator/index.js\r\n  2. Higher Order Functions -> ./features/hoc.js\r\n  3. Object.defineProperty(...) -> ./features/defineProperty\r\n  4. Object.assign and Spread Operator\r\n  5. Mediator Pattern -> ./mediator\r\n*/\n\n// Decorator();\n(0, _object2.default)();\n// ObjectDefineDemo();\n// Mediator();\n// classworks ->\n// Work1();\n\n//# sourceURL=webpack:///./application/index.js?");

/***/ }),

/***/ "./application/mediator/index.js":
/*!***************************************!*\
  !*** ./application/mediator/index.js ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\n\nObject.defineProperty(exports, \"__esModule\", {\n  value: true\n});\n\nvar _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if (\"value\" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();\n\nfunction _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError(\"Cannot call a class as a function\"); } }\n\nvar MediatorClasses = function MediatorClasses() {\n  var Flight = function () {\n    function Flight(name) {\n      _classCallCheck(this, Flight);\n\n      this.name = name;\n      this.controlRoom = null;\n\n      console.log('Flight ' + name + ' in progress');\n    }\n\n    _createClass(Flight, [{\n      key: 'receive',\n      value: function receive(message, from) {\n        console.log(from.name + ' to ' + this.name + ': ' + message);\n      }\n    }, {\n      key: 'send',\n      value: function send(message, to) {\n        if (this.controlRoom !== null) {\n          this.controlRoom.send(message, this, to);\n        } else {\n          console.warn(this.name + ' - you can\\'t send message, until you dont connected to control room');\n        }\n      }\n    }]);\n\n    return Flight;\n  }();\n  /*  - - - - - */\n\n  var Alpha = new Flight('Alpha');\n  var Bravo = new Flight('Bravo');\n  var Beta = new Flight('Beta');\n\n  console.log('- - - - - - - - -');\n\n  Alpha.receive('Welcome in Ukraine!', { name: 'SBU' });\n  Bravo.receive('Who are you?', { name: 'SBU' });\n  Bravo.send('Don\\'t shoot me pls!');\n\n  /* - - - - - */\n\n  var ControlRoom = function () {\n    function ControlRoom(name) {\n      _classCallCheck(this, ControlRoom);\n\n      this.name = name;\n      this.connectedFlights = {};\n    }\n\n    _createClass(ControlRoom, [{\n      key: 'register',\n      value: function register(flight) {\n        this.connectedFlights[flight.name] = flight;\n        flight.controlRoom = this;\n\n        console.log('New flight \\'' + flight.name + '\\' registered in control room ' + name);\n        console.log('List or registered:', this.connectedFlights);\n      }\n    }, {\n      key: 'send',\n      value: function send(message, from, to) {\n        // console.log(\n        //   'message:', message,\n        //   '\\nfrom:', from,\n        //   '\\nthis:', this,\n        //   '\\nconnectedFlights:', connectedFlights\n        // );\n        if (to !== undefined) {\n          to.receive(message, from);\n        } else {\n          for (var key in this.connectedFlights) {\n            if (this.connectedFlights[key] !== from) {\n              this.connectedFlights[key].receive(message, from);\n            }\n          }\n        } // else\n      } // send\n\n    }]);\n\n    return ControlRoom;\n  }(); // control room\n  /*  - - - - - */\n\n  var Borispol = new ControlRoom('Borispol');\n  console.log('- - - - - - - - -');\n  Borispol.register(Alpha);\n  Borispol.register(Bravo);\n  Borispol.register(Beta);\n  console.log('- - - - - - - - -');\n  Beta.send('Hello, Kyiv! How\\'s the weather?');\n  Alpha.send('Hello guys! It\\s rainy, probably we need go to Harkiv or Odessa');\n  Alpha.send('Shoot Beta! Probably he\\'s a terrorist!', Bravo);\n};\n\nexports.default = MediatorClasses;\n\n//# sourceURL=webpack:///./application/mediator/index.js?");

/***/ })

/******/ });